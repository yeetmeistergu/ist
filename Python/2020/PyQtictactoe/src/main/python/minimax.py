import sys
import platform
import time
from os import system
from random import choice
from math import inf as infinity

# An implementation of Minimax AI Algorithm in Tic Tac Toe using Python.
# This software is available under GPL license.
# Original code: https://github.com/Cledersonbc/tic-tac-toe-minimax/blob/master/py_version/minimax.py
# Author of original code: Clederson Cruz
# Year: 2017
# Adapted for this application by: Louie Gu
# Year: 2020
# License: GNU GENERAL PUBLIC LICENSE (GPL)

MAX = +1
MIN = -1

def evaluate(state):
    """
    Function for heuristic evaluation of state.
    :state: the state of the current board
    :return: +1 if the computer wins; -1 if the human wins; 0 draw
    """
    if wins(state, MIN):
        score = +1
    elif wins(state, MAX):
        score = -1
    else:
        score = 0

    return score

def wins(state, player):
    """
    This function tests if a specific player wins. Possibilities:
    * Three rows    [X X X] or [O O O]
    * Three cols    [X X X] or [O O O]
    * Two diagonals [X X X] or [O O O]
    :state: the state of the current board
    :player: a human or a computer
    :return: True if the player wins
    """
    win_state = [
        [state[0][0], state[0][1], state[0][2]],
        [state[1][0], state[1][1], state[1][2]],
        [state[2][0], state[2][1], state[2][2]],
        [state[0][0], state[1][0], state[2][0]],
        [state[0][1], state[1][1], state[2][1]],
        [state[0][2], state[1][2], state[2][2]],
        [state[0][0], state[1][1], state[2][2]],
        [state[2][0], state[1][1], state[0][2]],
    ]
    if [player, player, player] in win_state:
        return True
    else:
        return False

def game_over(state):
    """
    This function tests if the human or computer wins
    :state: the state of the current board
    :return: True if the human or computer wins
    """
    return wins(state, MAX) or wins(state, MIN)

def convertBoard(e):
    """
    Converts the board to Cledersonbc's
    (original author of the code) format, 
    which the algorithm uses
    """
    b = [[e[0],e[1],e[2]], [e[3],e[4],e[5]], [e[6],e[7],e[8]]]
    return b

def empty_cells(state):
    """
    Each empty cell will be added into cells list
    :state: the state of the current board
    :return: list of empty cells
    """
    cells = []

    # For every row, check the cell in every collumn.
    for x, row in enumerate(state):
        for y, cell in enumerate(row):
            if cell == 0:
                cells.append([x, y])

    return cells

def minimax(state, depth, player):
    """
    AI function that chooses the best move
    :state: current state of the board
    :depth: node index in the tree (0 <= depth <= 9),
    :player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
    if player == MIN:
        best = [-1, -1, -infinity]
    else:
        best = [-1, -1, +infinity]

    # For the recursion, returns a score for each move combination.
    if depth == 0 or game_over(state):
        score = evaluate(state)
        return [-1, -1, score]

    for cell in empty_cells(state):
        x, y = cell[0], cell[1]
        # We make every possible move from this state, and then see the outcome.
        state[x][y] = player
        # Recursion, plays every possible move from this situation and obtains the outcome for all the options.
        score = minimax(state, depth - 1, -player) 
        # We reset the cell after the calculations.
        state[x][y] = 0
        # We give the score to the cell being tested.
        score[0], score[1] = x, y

        if player == MIN:
            # We give player the better score.
            if score[2] > best[2]:
                best = score

        else:
            # We keep the good score for the player.
            if score[2] < best[2]:
                best = score 

    return best

def convertMove(best):
    """
    Converts the move to the format used in main.py
    """
    row, col = best[0], best[1]
    print("Preconversion moves:", row, col)

    if row == 0:
        if col == 0:
            bestMove = 0
        elif col == 1:
            bestMove = 1
        else:
            bestMove = 2
    elif row == 1:
        if col == 0:
            bestMove = 3
        elif col == 1:
            bestMove = 4
        else:
            bestMove = 5
    else:
        if col == 0:
            bestMove = 6
        elif col == 1:
            bestMove = 7
        else:
            bestMove = 8

    return bestMove
