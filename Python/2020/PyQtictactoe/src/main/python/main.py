# This Python file uses the following encoding: utf-8
import sys
import game_icons
import random
import minimax
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
from PyQt5 import uic
from game_ui import Ui_MainWindow

# A game of Tic Tac Toe written in Python and PyQt5, utilising an adapted version of Clederson Cruz's
# Minimax AI Algorithm.
# This software is available under GPL license.
# Original algorithm: https://github.com/Cledersonbc/tic-tac-toe-minimax/blob/master/py_version/minimax.py
# Author of original algorithm: Clederson Cruz
# Year: 2017
# Author of this application: Louie Gu
# Year: 2020
# License: GNU GENERAL PUBLIC LICENSE (GPL)

# Subclass QMainWindow to customise your application's main window
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # Importing the designed UI from Qt Designer
        # uic.loadUi("game.ui", self)
        # self.show()

        # Set up UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("PyQtictactoe")

        # Set up variables
        self.winner = ""
        self.turn = +1 #Crosses is +1, Noughts is -1. This is kept consistent in minimax.py
        self.game = False
        self.taken = 0
        self.mode = 0
        self.labels = []
        self.difficulty = 1

        # Set up menu items
        self.ui.actionNew_Game.triggered.connect(self.newAction)
        self.ui.actionExit_Game.triggered.connect(sys.exit)

        # Append labels to list
        self.labels.append(self.ui.g0)
        self.labels.append(self.ui.g1)
        self.labels.append(self.ui.g2)
        self.labels.append(self.ui.g3)
        self.labels.append(self.ui.g4)
        self.labels.append(self.ui.g5)
        self.labels.append(self.ui.g6)
        self.labels.append(self.ui.g7)
        self.labels.append(self.ui.g8)

        # Load some pixmaps
        self.pixmap0 = QPixmap(":/images/crosses.png")
        self.pixmap1 = QPixmap(":/images/noughts.png")

        # Display window
        self.show()

    def eventFilter(self, obj, event):
        # Check for clicks
        if event.type() == QEvent.MouseButtonRelease:

            # Only activate if game in progress
            if self.game == True:
                for i in range(9):
                    if obj == self.labels[i]:
                        self.checkMove(i)
                        return True

        return False

    # I couldn't figure out how to pass variables in button.connect() ¯\_(ツ)_/¯
    def newAction(self):
        self.newGame(1)

    def newGame(self, gm):
        print("Setting up game...")

        # If we don't want to keep settings
        if gm == 1:
            # Ask if 1 player or 2 player
            p = QMessageBox()
            p.setText("Select game mode:")
            p.setIcon(QMessageBox.Information)
            op = QPushButton("One Player")
            p.addButton(op, QMessageBox.NoRole)
            tp = QPushButton("Two Player")
            p.addButton(tp, QMessageBox.YesRole)
            p.setDefaultButton(op)
            p.exec_()
            option = p.clickedButton().text()

            if option == "One Player":
                self.mode = 0
                
                # Set difficlty
                d = QMessageBox()
                d.setText("Select difficulty:")
                d.setIcon(QMessageBox.Information)
                easy = QPushButton("Easy")
                normal = QPushButton("Normal")
                hard = QPushButton("Hard")
                d.addButton(easy, QMessageBox.ActionRole)
                d.addButton(normal, QMessageBox.NoRole)
                d.addButton(hard, QMessageBox.YesRole)
                d.setDefaultButton(normal)
                d.exec_()
                diff = d.clickedButton().text()

                if diff == "Easy":
                    self.difficulty = 0

                elif diff == "Normal":
                    self.difficulty = 1

                elif diff == "Hard":
                    self.difficulty = 2

            elif option == "Two Player":
                self.mode = 1
        
        print("Gamemode", self.mode)
        
        # Set up game "grid"
        self.board = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.winner = ""
        self.turn = +1
        self.taken = 0
        self.game = True

        # Rebuild Labels because for some reason QLabel().clear() does not seem to work.
        for i in self.labels:
            self.ui.gridLayout.removeWidget(i)
            i.deleteLater()
            i = None

        r = 0 # row
        c = 0 # collumn

        for i in range(9):
            self.labels[i] = QLabel(self.ui.centralwidget)
            self.labels[i].setText("")
            self.labels[i].setScaledContents(True)
            self.labels[i].setAlignment(Qt.AlignHCenter|Qt.AlignVCenter)
            self.labels[i].setIndent(0)
            self.labels[i].setObjectName("g"+str(i))
            self.labels[i].setAttribute(Qt.WA_TranslucentBackground, True)
            self.ui.gridLayout.addWidget(self.labels[i], r, c, 1, 1)

            # Setting rows and columns
            if c == 2:
                c = 0
                r += 1
            else:
                c += 1

        # Set up labels to be clickable
        for i in self.labels:
            i.installEventFilter(self)

    # Check if move is valid
    def checkMove(self, n):
        if self.board[n] == 0:
            print("Turn =", self.turn)
            self.board[n] = self.turn
            print(self.board)
            self.placeMove(n)
            return True
        else:
            print("Invalid Move", str(n))
            e = QMessageBox()
            e.setText("Invalid move!")
            e.setIcon(QMessageBox.Warning)
            e.exec_()
            return False

    def placeMove(self, n):
        if self.turn == +1:
            pm = self.pixmap0
        else:
            pm = self.pixmap1

        # Place the character
        e2 = self.labels[n]
        e1 = QLabel(e2)
        e1.setScaledContents(True)
        e1.setPixmap(pm)
        e1.show()
    
        self.taken += 1

        # Change the player
        if self.turn == +1:
            self.turn = -1

            if self.mode == 0:
                self.aiMove()

        elif self.turn == -1:
            self.turn = +1

        self.testWin()

    def aiMove(self):
        if self.mode == 0:

            if self.difficulty == 0:
                if self.taken < 9 and self.winner == "":

                    # Cycle through moves on the board
                    for i in range(9):
                        if self.board[i] == 0:
                            move = i
                            break

                    if move != None:
                        self.checkMove(move)
                    return True

            elif self.difficulty == 1:
                if self.taken < 9 and self.winner == "":

                    moving = True

                    # Cycling through random moves until it finds one that is valid
                    while moving:
                        r = random.random()
                        e = random.randint(0,8)
                        print("Random AI Move:", str(e))
                        if self.board[e] == 0:
                            self.checkMove(e)
                            moving = False
                            return True

            elif self.difficulty == 2:

                # Minimax AI
                mb = minimax.convertBoard(self.board)
                depth = len(minimax.empty_cells(mb))

                # If game not over, minimax time
                if depth != 0 or not minimax.game_over(mb) and self.taken < 9:
                    
                    print("Minimax Board:", mb)
                    # Move is the converted value from the minimax algorithm
                    move = minimax.convertMove(minimax.minimax(mb, depth, minimax.MIN))
                    print("AI Move:", str(move))

                    if move != None:
                        self.checkMove(move)
                    return True

        return False

    def testWin(self):
        
        # Code borrowed from https://codereview.stackexchange.com/questions/146344/tic-tac-toe-in-pyqt4
        # Credit to its original author
        winning = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
        print("Testing for winner...")

        # Test each [] in winning to see if there is a 3 in a row anywhere.
        for i in range(8):
            if self.board[winning[i][0]] == +1 and \
               self.board[winning[i][1]] == +1 and \
               self.board[winning[i][2]] == +1:
                self.winner = "X"

            if self.board[winning[i][0]] == -1 and \
               self.board[winning[i][1]] == -1 and \
               self.board[winning[i][2]] == -1:
                self.winner = "O"
        
        # If there is a winner, report it and end the game        
        if self.winner == "X" or self.winner == "O":
            self.game = False
            msg = "{} wins!  Play again?".format(self.winner)

        # If there's a draw, handle it here
        if self.winner == "" and self.taken == 9:
            self.game = False
            msg = "It's a draw!  Play again?"

        if self.winner != "" or self.taken == 9:
            choice = QMessageBox()
            choice.setText(msg)
            choice.setIcon(QMessageBox.Information)
            yes = QPushButton("Yes")
            change = QPushButton("Change Settings")
            no = QPushButton("No")
            choice.addButton(yes, QMessageBox.ActionRole)
            choice.addButton(change, QMessageBox.NoRole)
            choice.addButton(no, QMessageBox.YesRole)
            choice.setDefaultButton(yes)
            choice.exec_()
            c = choice.clickedButton().text()

            if c == "Yes":
                self.newGame(0)

            elif c == "Change Settings":
                self.newGame(1)
                
            elif c == "No":
                sys.exit()

        print("No winner yet.")
        return False

if __name__ == "__main__":
    # From boilerplate for fbs (fman build system)
    appctxt = ApplicationContext()      # 1. Call ApplicationContext()
    window = MainWindow()
    window.show()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)