# -*- mode: python -*-

block_cipher = None


a = Analysis(['/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/src/main/python/main.py'],
             pathex=['/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/target/PyInstaller'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=['/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/venv/lib/python3.6/site-packages/fbs/freeze/hooks'],
             runtime_hooks=['/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/target/PyInstaller/fbs_pyinstaller_hook.py'],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='PyQtictactoe',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          console=False , icon='/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/target/Icon.icns')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='PyQtictactoe')
app = BUNDLE(coll,
             name='PyQtictactoe.app',
             icon='/Users/louie.gu/Desktop/IST/Python/PyQtictactoe/target/Icon.icns',
             bundle_identifier='com.yeetmeistergu.pyqtictactoe')
