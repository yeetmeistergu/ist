# PyQuest

*"Quite possibly your worst nightmare."*

You are a circle, and red circles are out to get you. Brown circles are also trying to get you, but they are largely incompetent. Red circles will hunt you down.

Delete the red circles by collecting many money, and reaching the holy level (69).

## Controls and Mechanics

Movement = wasd or arrow keys. Can only move one grid at a time.
Attack = space
Use ability = e
Reload = Ctrl + R (NOT COMMAND on macOS)
Save = Ctrl + S (NOT COMMAND on macOS)

- Grass, forest and mountain can be traversed without effort.
- Water (dark blue) and lava (orange) require the respective abilities to traverse.
- The black void grids will kill you if you step on them.
- Magenta grids are health.
- Gold grids are money.
- White grids give you better (or worse) weapons
- Light blue grids give you abilities

The game MUST be run from its root directory (i.e. the PyQuest folder) or else it will break.
You start in the top grid (grid 0). Going off the screen vertically will move you to a different map (i.e. moving down will spawn you in map 1, and when in map 2, moving up will move you back to map 1).

### Weapons

- Sword: can only attack enemies directly next to player.
- Gun: Fires small bullets which do moderate damage.
- Laser: Fires a laser. Does quite some damage.

### Abilities

- Lava waders: walk through lava
- Water boots: walk through water
- Health: heals 30 helth (cooldown of 10 seconds)
