'''
Random Map File Generator
Original Author: Peter Howse
Bugs email: p.howse@tsc.nsw.edu.au

Adapted for PyQuest
by Louie Gu
Bugs are features.

Outputs a map file in the folder where the script
is run. File format:
x|y|terrain type|passable terrain boolean|item

# If you re-run the script, you will need to delete the
# old file first, otherwise it just keeps adding to the
# end of the file

The above "bug" has been adjusted by yours truly.
Will now overwrite existing maps.
'''

import random

# Define the type of terrain available and whether it is
# passable or not.
# NOTE: the first terrain type is the most common
terrain = [
    ["grass", True],
    ["mountain", True],
    ["water", False],
    ["forest", True],
    ["lava", False],
    ]

# Define the items of interest for your map
interest = ["pit", "money", "health", "weapon", "ability"]

# Dimensions of the map
# x = int(input("How many squares across? "))
# y = int(input("How many squares down? "))
x = 16
y = 12

# How much of the primary terrain do you want?
# grass = int(input("How much grassland (1: Some, 2: Many, 3: Lots)? "))
grass = random.Random().randint(2, 3)

# How many special items of interest do you want?
# intlevel = int(input("How interesting do you want the map 1: A little, 2: A bit, 3: Very)"))
intlevel = random.Random().randint(1, 2)

# This function determines what is on any particular
# square of the map
def make_terrain(thegrass, theinterest):
    t_pct = [40,60,80]      # modify to adjust liklihood of primary terrain
    i_pct = [5,10,15,20]    # modify to adjust liklihood of items

    # Terrain definitions
    x = random.randint(1,100)
    if x <= t_pct[thegrass]:
        mapTerrain = "grass"
        mapPass = True
    else:
        x = random.randint(1,len(terrain)-1)
        mapTerrain = terrain[x][0]
        mapPass = terrain[x][1]

    # Items
    x = random.randint(1,100)
    if x <= i_pct[theinterest]:
        x = random.randint(0, len(interest)-1)
        mapInterest = interest[x]
    else:
        mapInterest = "none"

    # Returns a list of terrain, passable and items
    return mapTerrain, mapPass, mapInterest

def run(n):
    for xx in range(x):
        for yy in range(y):
            maptext = ""
            t = make_terrain(grass-1, intlevel-1)
            maptext = str(xx)+"|"+str(yy)+"|"+t[0]+"|"+str(t[1])+"|"+t[2]+"\n"
            # print (maptext) # uncomment to debug
            
            # If map exists, delete it.
            if xx == 0 and yy == 0:
                with open("./saves/map" + str(n) + ".txt", "w") as myfile:
                    myfile.write(maptext)
            else:
                with open("./saves/map" + str(n) + ".txt", "a") as myfile:
                    myfile.write(maptext)         
    print("maps created")
