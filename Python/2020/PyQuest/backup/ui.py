from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        # Set up labels to be connected
        self.labels = []

        # General Setup
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle("PyQuest")
        MainWindow.resize(720,720)
        MainWindow.setMinimumSize(720, 720)
        MainWindow.setMaximumSize(720, 720)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.centralwidget.setObjectName("centralwidget")
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        MainWindow.setStyleSheet("QLabel{text-align: center};")

        # Setup Stacked Widgets
        self.StackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.StackedWidget.setGeometry(QtCore.QRect(0, 0, 720, 720))
        self.StackedWidget.setSizePolicy(sizePolicy)
        self.StackedWidget.setObjectName("StackedWidget")
        sizePolicy.setHeightForWidth(self.StackedWidget.sizePolicy().hasHeightForWidth())

        # Set up main menu
        self.mm = []
        self.MainMenu = QtWidgets.QWidget()
        self.MainMenu.setObjectName("MainMenu")
        self.MainMenu.setStyleSheet("background-color: black; color: white;")
        self.mm_title = QtWidgets.QLabel(self.MainMenu)
        self.mm_title.setGeometry(QtCore.QRect(0, 150, 720, 80))
        self.mm_title.setFont(self.fontSize(64, True))
        self.mm_title.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.mm_title.setAlignment(QtCore.Qt.AlignCenter)
        self.mm_title.setObjectName("mm_title")
        self.mm_title.setText("PyQuest")
        self.mm_start = QtWidgets.QLabel(self.MainMenu)
        self.mm_start.setGeometry(QtCore.QRect(0, 300, 720, 36))
        self.mm_start.setFont(self.fontSize(36, False))
        self.mm_start.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.mm_start.setAlignment(QtCore.Qt.AlignCenter)
        self.mm_start.setObjectName("mm_start")
        self.mm_start.setText("Start Game")
        self.mm.append(self.mm_start)
        self.mm_load = QtWidgets.QLabel(self.MainMenu)
        self.mm_load.setGeometry(QtCore.QRect(0, 360, 720, 36))
        self.mm_load.setFont(self.fontSize(36, False))
        self.mm_load.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.mm_load.setAlignment(QtCore.Qt.AlignCenter)
        self.mm_load.setObjectName("mm_load")
        self.mm_load.setText("Load Game")
        self.mm.append(self.mm_load)
        self.mm_options = QtWidgets.QLabel(self.MainMenu)
        self.mm_options.setGeometry(QtCore.QRect(0, 420, 720, 36))
        self.mm_options.setFont(self.fontSize(36, False))
        self.mm_options.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.mm_options.setAlignment(QtCore.Qt.AlignCenter)
        self.mm_options.setObjectName("mm_options")
        self.mm_options.setText("Options")
        self.mm.append(self.mm_options)
        self.mm_exit = QtWidgets.QLabel(self.MainMenu)
        self.mm_exit.setGeometry(QtCore.QRect(0, 480, 720, 36))
        self.mm_exit.setFont(self.fontSize(36, False))
        self.mm_exit.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.mm_exit.setAlignment(QtCore.Qt.AlignCenter)
        self.mm_exit.setObjectName("mm_exit")
        self.mm_exit.setText("Quit")
        self.mm.append(self.mm_exit)
        self.StackedWidget.addWidget(self.MainMenu)
        self.labels.append(self.mm)

        # Set up character creation
        self.cs = []
        self.cSelect = QtWidgets.QWidget()
        self.cSelect.setObjectName("cSelect")
        self.cSelect.setStyleSheet("background-color: black; color: white;")
        self.cSelect_title = QtWidgets.QLabel(self.cSelect)
        self.cSelect_title.setGeometry(QtCore.QRect(0, 50, 720, 80))
        self.cSelect_title.setFont(self.fontSize(48, False))
        self.cSelect_title.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.cSelect_title.setAlignment(QtCore.Qt.AlignCenter)
        self.cSelect_title.setObjectName("cSelect_title")
        self.cSelect_title.setText("Create Character")
        self.cSelect_next = QtWidgets.QLabel(self.cSelect)
        self.cSelect_next.setGeometry(QtCore.QRect(625, 200, 100, 250))
        self.cSelect_next.setFont(self.fontSize(100, False))
        self.cSelect_next.setAlignment(QtCore.Qt.AlignCenter)
        self.cSelect_next.setObjectName("cSelect_next")
        self.cSelect_next.setText(">")
        self.cs.append(self.cSelect_next)
        self.cSelect_prev = QtWidgets.QLabel(self.cSelect)
        self.cSelect_prev.setGeometry(QtCore.QRect(0, 200, 100, 250))
        self.cSelect_prev.setFont(self.fontSize(100, False))
        self.cSelect_prev.setAlignment(QtCore.Qt.AlignCenter)
        self.cSelect_prev.setObjectName("cSelect_prev")
        self.cSelect_prev.setText("<")
        self.cs.append(self.cSelect_prev)
        self.StackedWidget.addWidget(self.cSelect)
        self.labels.append(self.cs)

        # Set up main game
        self.mg = []
        self.MainGame = QtWidgets.QWidget()
        self.MainGame.setObjectName("MainMenu")
        self.StackedWidget.addWidget(self.MainGame)
        self.labels.append(self.mg)

        MainWindow.setCentralWidget(self.centralwidget)
        self.StackedWidget.setCurrentIndex(0)

    def fontSize(self, size, bold):
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(size)
        if bold:
            font.setBold(True)
        return font

class stackedWidget(object):
    self.StackedWidget.setGeometry(QtCore.QRect(0, 0, 720, 720))
    self.StackedWidget.setSizePolicy(sizePolicy)
    self.StackedWidget.setObjectName("StackedWidget")