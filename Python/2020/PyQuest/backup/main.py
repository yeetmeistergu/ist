from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *

import sys
import os
import ui
import random

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # UI Setup
        self.ui = ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

        self.repaint()

        self.activeGrid = []

        # Set up game
        self.health = 100
        self.orbs = 0
        self.level = 1
        self.mp = 1 # Power multiplier
        self.character = random.Random().randint(0, 2)

        # Setup events for labels
        for i in self.ui.labels:
            for x in i:
                x.installEventFilter(self)

        # Set up threading
        self.threadpool = QThreadPool()

    # Placeholder function for development
    def placeholder(self):
        print("todo")

    # Process inputs
    def eventFilter(self, obj, event):

        # Check for clicks
        if event.type() == QEvent.MouseButtonRelease:

            if obj in self.ui.mm:
                mm = self.ui.mm
                if obj == mm[0]:
                    self.ui.StackedWidget.setCurrentIndex(1)
                    self.repaint()
                    self.initGame()
                elif obj == mm[1]:
                    self.loadGame()
                elif obj == mm[2]:
                    self.placeholder()
                elif obj == mm[3]:
                    sys.exit()
            
            elif obj in self.ui.cs:
                cs = self.ui.cs
                if obj == cs[0]:
                    if self.character < 3:
                        self.character += 1
                    else:
                        self.character = 0
                elif obj == cs[1]:
                    if self.character > 0:
                        self.character -= 1
                    else:
                        self.character = 3
                self.updateGame()
                # print(self.character)

        return False

    def initGame(self):
        self.updateGame()
        print("todo")

    def updateGame(self):
        worker = Worker(self.repaint)
        self.threadpool.start(worker)
        print("Redrawing Screen")

    # code yoinked from trading game
    def loadGame(self):
        f, ok = QFileDialog.getOpenFileName(self, "Select Save File Location", "~/PyQuest/save.pq", "Saves (*.pq)")
        if ok:
            try: 
                with open(f, "r") as ff:
                    r = csv.reader(ff)
                    l = list(r)
                    ''' 
                    self.country = int(l[0][0])
                    # Converts the string list into real list, and then compares the names to add the country to the
                    # self.unlocked list. 
                    # Reference: https://www.geeksforgeeks.org/python-convert-a-string-representation-of-list-into-list/
                    for i in self.countries:
                        if i.name in l[0][1].strip('][').split(', '):
                            self.unlocked.append(i)
                    self.money = round(float(l[0][2]))
                    self.unit = int(l[0][3])
                    # Converts string list to real list, and then converts its contents to int.
                    self.inventory = l[0][4].strip('][').split(', ')
                    for i in range(6):
                        self.inventory[i]
                        self.inventory[i] = int(self.inventory[i])
                    self.debt = round(float(l[0][5]))
                    self.storedMoney = round(float(l[0][6]))
                    '''
                msgBox = QMessageBox()
                msgBox.setText("Success! Save loaded.")
                msgBox.exec()
                self.ui.StackedWidget.setCurrentIndex(2)
            except:
                    msgBox = QMessageBox()
                    msgBox.setText("Error reading file.")
                    msgBox.exec()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        # if self.ui.StackedWidget.currentIndex() == 1:
        #     if self.character == 0:
        qp.setPen(QPen(Qt.white, 20, Qt.SolidLine))
        qp.setBrush(QBrush(Qt.white, Qt.SolidPattern))
        qp.drawRect(0, 0, 700, 700)

        qp.end()



class Worker(QRunnable):

    def __init__(self, function, *args, **kwargs):
        super(Worker, self).__init__()
        self.function = function # function passed through to the thread
        self.args = args
        self.kwargs = kwargs

    # Worker Thread
    @pyqtSlot()
    def run(self):
        self.function(*self.args, **self.kwargs)
        


        
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.resize(800, 800)
    window.resize
    sys.exit(app.exec_())