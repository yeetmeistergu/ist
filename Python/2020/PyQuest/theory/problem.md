# Problem Definition

We have identified that in the Tile Based Role Playing Game market, there is a lack of games with their own unique style. There seems to have been a shift away from true grid/tile based RPGs outside of turn based games. In addition, current existing solutions are part of a large series, and include a long term fanbase, and several layers of lore, and complex mechanics which have been developed through the many titles which make up those series. As such, it is very difficult for casual players to find a game in this market.

As such, we have identified a recipe for success in this market. To do this, we must create a game which is simple, and down to the roots of the Tile Based Role Playing Game genre. This product will need to have basic features done right, such as player movement; enemy movement; a combat system which is elegant, yet not too complex; unique weapons, items and abilities; a variety of characters and level mechanics; and a variety of mobs and challenges to tackle, without too much lore, and complex mechanisms involved in these. In addition, basic saving and loading of progress, and map generation and compatibility need to be included.

To complete this task, a well organised team is needed, and a clear cut schedule, gantt chart, data dictionary, and a modular code are the keys to the success of this project.

## Data Dictionary

|  Variable  |  Data Type  | Description |
|---|---|---|
|self.gaming|boolean|Runs game loop while True|
|self.black|tuple (integers)|The RGB values for the colour black
|self.enemies|array (objects)|Stores the enemies who are alive
|self.combatTimer|integer|Counts the time between each enemy attack
|self.shot|boolean|Determines if there should be bullets on screen
|self.lasers|boolean|Determines if there should be lasers on screen
|self.laser|array (objects)|Stores the lasers which have been shot
|self.bullets|array (objects)|Stores the bullets which have been shot
|self.activeGrid|integer|Stores the index of the currently active grid
|self.newGame|boolean|Determines if in the character selection screen
