import pygame
import os, sys, time, random, pickle
import mapmaker

# Class for player character
class Boi(object):
    '''
    The player class.
    Parameters:
    self
    x: player x coordinate 
    y: player y coordinate
    health: player health
    mp: player multiplyer (level)
    char: player index
    colour: player colour
    '''
    def __init__(self, x, y, health, mp, char, colour):
        self.x = x
        self.y = y
        self.health = health
        self.mp = mp
        self.colour = colour
        self.money = 0
        self.dead = False
        self.weapons = ["sword", "gun", "laser"]
        self.ability = ["lava", "water", "heal"]
        self.hasAbility = False
        self.activeAbility = random.Random().randint(0,2)
        self.abCooldown = 0
        self.onGrid = 0

        if char < 3:
            self.activeWeapon = char
        else:
            self.activeWeapon = char - 3

        self.draw()
    
    def draw(self):
        self.centre = (((self.x*50) + 25), ((self.y*50) + 25))
        self.model = pygame.draw.circle(screen, self.colour, (self.centre), 25)

# Enemies
class BadBoi(object):
    '''
    The enemy class.
    Parameters:
    self
    x: enemy x coordinate
    y: enemy y coordinate
    health: enemy health
    mp: enemy damage multiplyer
    vel: determines if an enemy moves randomly, or towards the player
    colour: enemy colour
    '''
    def __init__(self, x, y, health, mp, vel, colour):

        self.x = int(x)
        self.y = int(y)
        self.health = health*mp
        self.mp = mp
        self.vel = vel
        self.colour = colour
        self.moved = 0
        self.inCombat = False
        self.draw()
    
    def draw(self):
        self.centre = ((self.x*50) + 25), ((self.y*50) + 25)
        self.model = pygame.draw.circle(screen, self.colour, (self.centre), 25)

    def move(self, xx, yy, grid, ):
        '''
        Moves the enemies.
        Parameters:
        self
        xx = Player's x coords
        yy = Player's y coords
        grid = The current active grid
        '''
        testing = True
        passed = False

        if self.moved == 0:

            x = 0
            y = 0
            # Randomised Movement
            if self.vel == 1:
                x = random.Random().randint(-1, 1)
                y = random.Random().randint(-1, 1)
            
            else:
                # Moving towards player
                x_m = self.x - xx
                y_m = self.y - yy
                # print(str(x_m) + " " + str(y_m))

                if x_m > 0:
                    x = -1
                elif x_m < 0:
                    x = 1
                
                if y_m > 0:
                    y = -1
                elif y_m < 0:
                    y = 1

            if self.x + x >= 0 and self.y + y >= 0:
                if self.x + x <= 15:
                    if self.y + y <= 11:
                        passed = True
                        if self.x + x == xx:
                            if self.y + y == yy:
                                y = random.Random().randint(-1, 1)
                                passed = False
                            else:
                                passed = True
                    else:
                        y = random.Random().randint(-2, -1)
                else:
                    x = random.Random().randint(-2, -1)
                
                if passed:
                    if grid.grid[self.x + x][self.y + y][1] == "True":
                        self.x += x
                        self.y += y
                        self.moved += 1

                if not passed:
                    x = random.Random().randint(-2, 2)
                    y = random.Random().randint(-2, 2)
                    # print(str(self.x + x) + str(self.y + y))

            # print("moving")

        # Move every x frames
        elif self.moved < 30:
            self.moved += 1
            # print("not moving")

        else: 
            self.moved = 0

class Bullet(object):
    '''
    The bullets from the gun.
    Parameters:
    self
    x: bullet x coordinate
    y: bullet y coordinate
    dir: direction the bullet is travelling as an integer. 
         0 = up; 1 = down; 2 = left; 3 = right
    '''
    def __init__(self, x, y, dir):
        self.x = x
        self.y = y
        self.direction = dir
        self.draw()
    
    def draw(self):
        self.model = pygame.draw.rect(screen, (255, 255, 255), (self.x, self.y, 10, 10))

class Laser(object):
    '''
    The lasers.
    Parameters:
    self
    x: laser x coordinate
    y: laser y coordinate
    dir: direction the laser is travelling as an integer. 
         0 = up; 1 = down; 2 = left; 3 = right
    '''
    def __init__(self, x, y, dir):
        self.x = x
        self.y = y
        self.direction = dir
        self.draw()
    
    def draw(self):
        if self.direction < 2:
            self.model = pygame.draw.rect(screen, (255, 255, 255), (self.x, self.y, 10, 50))
        else:
            self.model = pygame.draw.rect(screen, (255, 255, 255), (self.x, self.y, 50, 10))

# Generate the grid from map files
class Grid():
    '''
    Generates a grid from existing map files.
    If no map files exist, creates a new set
    of map files using mapmaker.py
    Parameters:
    self
    n: The index of the map file/grid being
       created.
    '''
    def __init__(self, n): # *args, **kwargs):
        # self.args = args
        # self.kwargs = kwargs
        self.grid = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
        self.newGrid(n)
    
    # Attempts to load existing maps
    def newGrid(self, n):
        try:
            mapfile = open("./saves/map" + str(n) + ".txt", "r")
        except:
            print("No maps found, creating new.")
            mapmaker.run(n)
            mapfile = open("./saves/map" + str(n) + ".txt", "r")

        for line in mapfile:
            l = line.split("|")
            properties = [l[2], l[3], l[4].rstrip()]
            self.grid[int(l[0])].append(properties)
            # print(self.grid[int(l[0])][int(l[1])])
        # print("grid complete")

class Main():
    def __init__(self):
        self.clock = pygame.time.Clock()
        self.gaming = True

        # # Threading
        # self.q = GameQueue(workers=4)

        # Setting up colours
        self.black = (0,0,0)
        self.white = (255, 255, 255)
        self.red = (255,0,0)
        self.green = (0,255,0)
        self.darkGreen = (22, 140, 22)
        self.blue = (0,0,255)
        self.yellow = (255,255,0)
        self.cyan = (0, 255, 255)
        self.magenta = (255, 0, 255)
        self.orange = (255, 165, 0)
        self.darkOrange = (255, 85, 0)
        self.purple = (201, 71, 245)
        self.grey = (200, 200, 200)
        self.brown = (165, 42, 42)
        self.gold = (255, 210, 0)

        # Enemies
        self.enemies = []
        self.combatTimer = 0

        # Laser/gun mechanics
        self.shot = False
        self.laser = []
        self.lasers = False
        self.bullets = []

        # Player Variables
        self.char = random.Random().randint(0,5)
        self.inCombat = False
        self.activeGrid = 0

        # Scenes
        self.newGame = False
        self.options = False
        self.game = False
        self.inventory = False
        self.MainMenu = True

        while self.gaming:
            self.clock.tick(30)
            # print(self.clock.get_fps())
            pygame.display.update()

            # Main menu
            if self.MainMenu:
                font1 = pygame.font.SysFont("Comic Sans MS", 72) 
                font2 = pygame.font.SysFont("Comic Sans MS", 42)
                title = font1.render("PyQuest", True, self.white)
                screen.fill(self.black)
                m_y = 0
                screen.blit(title, ((800 - font1.size("PyQuest")[0])/2,0))
                m_y += font1.size("PyQuest")[1] + 60
                play = screen.blit(font2.render("New Game", True, self.white), ((800 - font2.size("New Game")[0])/2, int(m_y)))
                m_y += 75
                load = screen.blit(font2.render("Load Game", True, self.white), ((800 - font2.size("Load Game")[0])/2,int(m_y)))
                m_y += 75
                option = screen.blit(font2.render("Options", True, self.white), ((800 - font2.size("Options")[0])/2,int(m_y)))
                m_y += 75
                leave = screen.blit(font2.render("Exit Game", True, self.white), ((800 - font2.size("Exit Game")[0])/2,int(m_y)))

            # Character Creation
            elif self.newGame:
                font1 = pygame.font.SysFont("Comic Sans MS", 60) 
                font2 = pygame.font.SysFont("Comic Sans MS", 200)
                font3 = pygame.font.SysFont("Comic Sans MS", 48)
                title = font1.render("Create Character", True, self.white)
                screen.fill(self.black)
                screen.blit(title, ((800 - font1.size("Create Character")[0])/2,0))
                c_y = 250 - (font2.size(">")[1]/2)
                prevChar = screen.blit(font2.render("<", True, self.white), (10 , c_y ))
                nextChar = screen.blit(font2.render(">", True, self.white), (790 - font2.size(">")[0], c_y ))
                startButton = screen.blit(font3.render("Start", True, self.white), ((800 - font3.size("Start")[0])/2, 450))
                charColour = [self.white, self.yellow, self.orange, self.magenta, self.cyan, self.purple]
                character = pygame.draw.circle(screen, charColour[self.char], (400, 275), 50)                

            # Main game loop
            elif self.game:
                if not self.player.dead:
                    self.regrid()
                    self.checkCombat()
                    self.createEnemies()
                    self.checkDeath()
                    self.manageThings()
                    if self.shot:
                        self.shootBullet()
                    if self.lasers:
                        self.shootLaser()

            # Event Filtering
            for event in pygame.event.get():
                # Quit Function
                if event.type == pygame.QUIT:
                    self.gaming = False
                    # try:
                    #     os.system("rm ./saves/*") # for debugging. deleting every save/map file
                    # except:
                    #     os.system("del .\saves\*")
                    # else:
                    #     print("something broked")

                # Mouse clicks
                elif event.type == pygame.MOUSEBUTTONUP:
                    mpos = pygame.mouse.get_pos()

                    if self.MainMenu:
                        if play.collidepoint(mpos):
                            self.MainMenu = False
                            self.newGame = True

                        elif load.collidepoint(mpos):
                            self.MainMenu = False
                            self.loadGame()
                            self.game = True

                        elif option.collidepoint(mpos):
                            print("there are no options")

                        elif leave.collidepoint(mpos):
                            pygame.quit()
                            quit()

                    elif self.newGame:
                        if prevChar.collidepoint(mpos):
                            if self.char > 0:
                                self.char -= 1
                            else:
                                self.char = 5

                        elif nextChar.collidepoint(mpos):
                            if self.char < 5:
                                self.char += 1
                            else:
                                self.char = 0

                        elif startButton.collidepoint(mpos):
                            self.newGame = False
                            self.game = True
                            self.activeGrid = 0
                            self.grid = [Grid(0), Grid(1), Grid(2)]
                            self.laser = []
                            self.bullets = []
                            xx = random.Random().randint(0, 15)
                            yy = random.Random().randint(0, 11)
                            testing = True

                            while testing:
                                if self.grid[self.activeGrid].grid[xx][yy][0] == "grass" and self.grid[self.activeGrid].grid[xx][yy][2] == "none":
                                    testing = False
                                else:
                                    xx = random.Random().randint(0, 15)
                                    yy = random.Random().randint(0, 11)

                            self.player = Boi(xx, yy, 100, 1, self.char, charColour[self.char])

                    elif self.player.dead:
                        if self.restart.collidepoint(mpos):
                            self.newGame = False
                            self.options = False
                            self.game = False
                            self.inventory = False
                            self.MainMenu = True
                            self.inCombat = False
                            self.enemies = []
                            self.laser = []
                            self.bullets = []
                            del self.player

                elif event.type == pygame.KEYDOWN:
                    
                    if event.mod == pygame.KMOD_NONE:
                        if event.key == pygame.K_ESCAPE:
                            pygame.quit()
                            quit()

                        elif self.game and not self.player.dead: 
                            # Moves players if the grid moving to is passable
                            if not self.inCombat:
                                if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                                    if self.player.x > 0:
                                        if self.grid[self.activeGrid].grid[self.player.x - 1][self.player.y][1] == "True":
                                            self.player.x -= 1
                                        # else:
                                        #     print(self.grid[self.activeGrid].grid[self.player.x - 1][self.player.y])
                                
                                if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                                    if self.player.x < 15:
                                        if self.grid[self.activeGrid].grid[self.player.x + 1][self.player.y][1] == "True":
                                            self.player.x += 1

                                if event.key == pygame.K_UP or event.key == pygame.K_w:
                                    if self.player.y > 0:
                                        if self.grid[self.activeGrid].grid[self.player.x][self.player.y - 1][1] == "True":
                                            self.player.y -= 1
                                    elif self.activeGrid > 0:
                                        if self.grid[self.activeGrid - 1].grid[self.player.x][11][1] == "True":
                                            self.activeGrid -= 1
                                            self.player.y = 11

                                if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                                    if self.player.y < 11:
                                        if self.grid[self.activeGrid].grid[self.player.x][self.player.y + 1][1] == "True":
                                            self.player.y += 1
                                    elif self.activeGrid < 2:
                                        if self.grid[self.activeGrid + 1].grid[self.player.x][0][1] == "True":
                                            self.activeGrid += 1
                                            self.player.y = 0
                            
                            if event.key == pygame.K_SPACE:
                                self.attack()
                                
                            elif event.key == pygame.K_e:
                                self.useAbility()

                    # Control not command which is really irritating.
                    elif event.mod & pygame.KMOD_CTRL:
                        if event.key == pygame.K_s:
                            self.saveGame()
                            print("Saved.")
                        elif event.key == pygame.K_r:
                            self.newGame = False
                            self.options = False
                            self.game = False
                            self.inventory = False
                            self.MainMenu = True
                            self.inCombat = False
                            self.enemies = []
                            del self.player

            
        pygame.quit()
        quit()

    # Redraw the grid
    def regrid(self):
        screen.fill(self.white)
        g_x = 0
        g_y = 0
        
        # For every x value
        for i in range(16):
            # For every y value
            for e in range(12):
                l = self.grid[self.activeGrid].grid[i][e]
                x = i*50
                y = e*50
                terrain = l[0]
                spec = l[2]
                passable = l[1]

                # Draw the basic terrain
                if terrain == "grass":
                    pygame.draw.rect(screen, self.green, (x, y, 50, 50))

                elif terrain == "mountain":
                    pygame.draw.rect(screen, self.grey, (x, y, 50, 50))
                    
                elif terrain == "water":
                    pygame.draw.rect(screen, self.blue, (x, y, 50, 50))
                    # Check if player has water boots
                    if self.player.hasAbility and self.player.ability[self.player.activeAbility] == "water":
                        self.grid[self.activeGrid].grid[i][e][1] = "True"
                    else:
                        self.grid[self.activeGrid].grid[i][e][1] = "False"

                elif terrain == "forest":
                    pygame.draw.rect(screen, self.darkGreen, (x, y, 50, 50))

                elif terrain == "lava":
                    pygame.draw.rect(screen, self.darkOrange, (x, y, 50, 50))
                    # Check if player has lava boots
                    if self.player.hasAbility and self.player.ability[self.player.activeAbility] == "lava":
                        self.grid[self.activeGrid].grid[i][e][1] = "True"
                    else:
                        self.grid[self.activeGrid].grid[i][e][1] = "False"

                # Place items and traps
                if spec != "none":
                    if passable == "True":
                        if spec == "pit":
                            pygame.draw.rect(screen, self.black, (x, y, 50, 50))

                        elif spec == "money":
                            pygame.draw.rect(screen, self.gold, (x, y, 50, 50))

                        elif spec == "health":
                            pygame.draw.rect(screen, self.magenta, (x, y, 50, 50))

                        elif spec == "weapon":
                            pygame.draw.rect(screen, self.white, (x, y, 50, 50))

                        elif spec == "ability":
                            pygame.draw.rect(screen, self.cyan, (x, y, 50, 50))
                    else:
                        self.grid[self.activeGrid].grid[i][e][2] == "none"                

        self.player.draw()

        for e in self.enemies:
            e.draw()
            if not e.inCombat:
                e.move(self.player.x, self.player.y, self.grid[self.activeGrid])

        # Draw the HUD
        font = pygame.font.SysFont("Comic Sans MS", 36)
        helthbar = screen.blit(font.render("Helth: " + str(self.player.health), True, self.white), (0,0))
        level = screen.blit(font.render("Player Level: " + str(self.player.mp), True, self.white), (0,600-font.size("Player")[1]))
        moneys = screen.blit(font.render("Money: " + str(self.player.money), True, self.white), (800-font.size("Money: " + str(self.player.money))[0],0))
        if self.player.hasAbility:
            abil = screen.blit(font.render("Ability: " + str(self.player.ability[self.player.activeAbility]), True, self.white), (800-font.size("Ability: " + str(self.player.ability[self.player.activeAbility]))[0], 600-font.size("Ability")[1]))

    def checkDeath(self):
        if self.grid[self.activeGrid].grid[self.player.x][self.player.y][2] == "pit" or self.player.health <= 0:
            self.player.dead = True
            self.bullets.clear()
            self.laser.clear()
            pygame.draw.rect(screen, self.black, (100, 100, 600, 400))
            font1 = pygame.font.SysFont("Comic Sans MS", 72) 
            font2 = pygame.font.SysFont("Comic Sans MS", 42)
            title = font1.render("You Died.", True, self.white)
            screen.blit(title, ((800 - font1.size("You Died")[0])/2,150))
            self.restart = screen.blit(font2.render("Return to Menu", True, self.white), ((800 - font2.size("Return to Menu")[0])/2, 300))
            self.player.health = 0
    
    def checkWin(self):
        if self.player.mp == 69 or self.player.money == 420:
            self.player.dead = True
            self.bullets.clear()
            self.laser.clear()
            pygame.draw.rect(screen, self.black, (100, 100, 600, 400))
            font1 = pygame.font.SysFont("Comic Sans MS", 72) 
            font2 = pygame.font.SysFont("Comic Sans MS", 42)
            title = font1.render("You Win!!!", True, self.white)
            screen.blit(title, ((800 - font1.size("You Win!!!")[0])/2,150))
            self.restart = screen.blit(font2.render("thank", True, self.white), ((800 - font2.size("thank")[0])/2, 300))
            self.player.health = 0

    def createEnemies(self):
        # Clean up enemies which are meant to be dead
        for e in self.enemies:
            if e.health <= 0:
                del self.enemies[self.enemies.index(e)]
                if self.inCombat:
                    self.inCombat == False
                self.player.health += e.mp
                self.player.money += e.mp

        r = random.Random().randint(2,4)
        if len(self.enemies) < r and self.player.mp < 50:

            # Random spawnpoint
            xx = random.Random().randint(0, 15)
            yy = random.Random().randint(0, 11)
            testing = True

            # Test if spawnpoint is valid
            while testing:
                if self.grid[self.activeGrid].grid[xx][yy][0] == "grass" and self.grid[self.activeGrid].grid[xx][yy][2] == "none" and xx != self.player.x and yy != self.player.y:
                    testing = False
                else:
                    xx = random.Random().randint(0, 15)
                    yy = random.Random().randint(0, 11)

            vel = random.Random().randint(0,2)

            if vel == 1:
                col = self.brown
            else:
                col = self.red
            
            e = BadBoi(xx, yy, random.Random().randint(50, 150), random.Random().randint(self.player.mp, self.player.mp + 1), vel, col)

            self.enemies.append(e)

    def checkCombat(self):

        combatant = 0

        for e in range(len(self.enemies)):
            p = self.player

            # Check if enemy next to player
            if self.enemies[e].x == p.x - 1 or self.enemies[e].x == p.x + 1:
                if self.enemies[e].y == p.y:

                    # Players take damage every second when directly next to enemy
                    # Damage scales with level.
                    if self.enemies[e].health >= 0:
                        # combatant = e
                        # self.enemies[e].inCombat = True
                        # self.inCombat = True
                        # break
                        # if self.inCombat:
                            if self.combatTimer == 0:
                                self.player.health -= 10*self.enemies[combatant].mp
                                self.combatTimer += 1
                            elif self.combatTimer >= 30:
                                self.combatTimer = 0
                            else:
                                self.combatTimer += 1

                            # print(self.combatTimer)
                    elif self.enemies[e].health <= 0:
                        # self.inCombat = False
                        self.player.money += self.enemies[e].mp
                        self.player.health += int(self.enemies[e].mp)
                        del self.enemies[e]
                        break

            elif self.enemies[e].y == p.y + 1 or self.enemies[e].y == p.y - 1:
                if self.enemies[e].x == p.x:

                    if self.enemies[e].health >= 0:
                        # combatant = e
                        # self.enemies[e].inCombat = True 
                        # self.inCombat = True
                        # break
                        # if self.inCombat:
                            if self.combatTimer == 0:
                                self.player.health -= 10*self.enemies[combatant].mp
                                self.combatTimer += 1
                            elif self.combatTimer >= 30:
                                self.combatTimer = 0
                            else:
                                self.combatTimer += 1
                            # print(self.combatTimer)

                    elif self.enemies[e].health <= 0:
                        # self.inCombat = False
                        self.player.money += self.enemies[e].mp
                        self.player.health += int(self.enemies[e].mp)
                        del self.enemies[e]
                        break

            

    def attack(self):
        # print(self.player.activeWeapon)
        item = self.player.weapons[self.player.activeWeapon]

        if item == "sword":
            # Draws the weapon
            u = pygame.draw.rect(screen, self.white, (self.player.x*50 + 20, self.player.y*50 - 50, 10, 50))
            d = pygame.draw.rect(screen, self.white, (self.player.x*50 + 20, self.player.y*50 + 50, 10, 50))
            l = pygame.draw.rect(screen, self.white, (self.player.x*50 - 50, self.player.y*50 + 20, 50, 10))
            r = pygame.draw.rect(screen, self.white, (self.player.x*50 + 50, self.player.y*50 + 20, 50, 10))
            weapon = [u,d,l,r]

            # Check for collision with enemy
            for i in self.enemies:
                damage = False
                left = (i.centre[0] - 25, i.centre[1])
                right = (i.centre[0] + 25, i.centre[1])
                top = (i.centre[0], i.centre[1] - 25)
                bottom = (i.centre[0], i.centre[1] + 25)
            
                for w in weapon:
                    if w.collidepoint(left) or w.collidepoint(right) or w.collidepoint(top) or w.collidepoint(bottom):
                        damage = True
                
                if damage:
                    i.health -= 10*self.player.mp
                    # print(i.health)

        elif item == "gun":
            # Draws the bullets
            u = Bullet(self.player.x*50 + 20, self.player.y*50 - 10, 0)
            d = Bullet(self.player.x*50 + 20, self.player.y*50 + 50, 1)
            l = Bullet(self.player.x*50 - 10, self.player.y*50 + 20, 2)
            r = Bullet(self.player.x*50 + 50, self.player.y*50 + 20, 3)
            self.bullets.append(u)
            self.bullets.append(d)
            self.bullets.append(l)
            self.bullets.append(r)
            self.shot = True
        
        elif item == "laser":
            #Draws the lasers
            u = Laser(self.player.x*50 + 20, self.player.y*50 - 50, 0)
            d = Laser(self.player.x*50 + 20, self.player.y*50 + 50, 1)
            l = Laser(self.player.x*50 - 50, self.player.y*50 + 20, 2)
            r = Laser(self.player.x*50 + 50, self.player.y*50 + 20, 3)
            self.laser.append(u)
            self.laser.append(d)
            self.laser.append(l)
            self.laser.append(r)
            self.lasers = True

    def shootBullet(self):
        # Moves the bullets
        if self.bullets == []:
            self.shot = False

        for m in self.bullets:
            if m.direction == 0:
                m.y -= 10
            elif m.direction == 1:
                m.y += 10
            elif m.direction == 2:
                m.x -= 10
            elif m.direction == 3:
                m.x += 10
            m.draw()

        for i in self.enemies:
            damage = False
            left = (i.centre[0] - 25, i.centre[1])
            right = (i.centre[0] + 25, i.centre[1])
            top = (i.centre[0], i.centre[1] - 25)
            bottom = (i.centre[0], i.centre[1] + 25)
        
            # Delets bullets if it contacts enemy
            for b in self.bullets:
                if b.x == 0 or b.y == 0:
                    del self.bullets[self.bullets.index(b)]
                elif b.model.collidepoint(left) or b.model.collidepoint(right) or b.model.collidepoint(top) or b.model.collidepoint(bottom):
                    damage = True
                    del self.bullets[self.bullets.index(b)]
                    # print("bullet deleted")
            
            if damage:
                i.health -= 20*self.player.mp
                # print(i.health)

    def shootLaser(self):
        # Moves the lasers
        if self.laser == []:
            self.lasers = False

        for m in self.laser:
            if m.direction == 0:
                m.y -= 10
            elif m.direction == 1:
                m.y += 10
            elif m.direction == 2:
                m.x -= 10
            elif m.direction == 3:
                m.x += 10
            m.draw()

        # Deletes lasers if it contacts an enemy
        for i in self.enemies:
            damage = False
            left = (i.centre[0] - 25, i.centre[1])
            right = (i.centre[0] + 25, i.centre[1])
            top = (i.centre[0], i.centre[1] - 25)
            bottom = (i.centre[0], i.centre[1] + 25)
        
            for b in self.laser:
                if b.x == 0 or b.y == 0:
                    del self.laser[self.laser.index(b)]
                elif b.model.collidepoint(left) or b.model.collidepoint(right) or b.model.collidepoint(top) or b.model.collidepoint(bottom):
                    damage = True
                    del self.laser[self.laser.index(b)]
                    # print("laser deleted")
            
            if damage:
                i.health -= 40*self.player.mp
                # print(i.health)

    def useAbility(self):
        if self.player.activeAbility == 2:
            if self.player.abCooldown == 0 and self.player.health <= 70:
                self.player.health += 30

        if self.player.abCooldown > 0:
            if self.player.abCooldown == 300:
                self.player.abCooldown = 0
            else:
                self.player.abCooldown += 1

    # Using pickle to save the player and its values as an object, and the loading it back in.
    def saveGame(self):
        # try:
            self.player.onGrid = self.activeGrid
            with open("./saves/player.pq", "wb") as f:
                pickle.dump(self.player, f)
        # except:
        #     try:
        #         os.system("mkdir ./saves/")
        #     except:
        #         print("error")

    def loadGame(self):
        try:
            with open("./saves/player.pq", "rb") as f:
                self.player = pickle.load(f)
            self.loadGame == False
            self.activeGrid = self.player.onGrid
            self.grid = [Grid(0), Grid(1), Grid(2)]
            self.laser = []
            self.bullets = []
            self.game = True

        except:
            print("no valid save files found")
            self.MainMenu = True

    # Misc handling of things
    def manageThings(self):
        # If player has landed on a special square, do something
        special = self.grid[self.activeGrid].grid[self.player.x][self.player.y][2]

        if special == "money":
            self.player.money += 1
            self.player.health += 1
            self.grid[self.activeGrid].grid[self.player.x][self.player.y][2] = "none"
        elif special == "health":
            self.player.health += 5
            self.grid[self.activeGrid].grid[self.player.x][self.player.y][2] = "none"
        elif special == "weapon":
            if self.player.activeWeapon < 2:
                self.player.activeWeapon += 1
            else:
                self.player.activeWeapon = 0
            self.grid[self.activeGrid].grid[self.player.x][self.player.y][2] = "none"
        elif special == "ability":
            if self.player.hasAbility:
                if self.player.activeAbility < 2:
                    self.player.activeAbility += 1
                else:
                    self.player.activeAbility = 0
            else:
                self.player.hasAbility = True
            self.grid[self.activeGrid].grid[self.player.x][self.player.y][2] = "none"

        # Level up if player has collected enough money
        if self.player.money >= 10*self.player.mp:
            self.player.money -= 10*self.player.mp
            self.player.mp += 1
    
if __name__ == "__main__":
    # Initialising
    pygame.init()
    screen = pygame.display.set_mode((800,600))
    pygame.display.set_caption('PyQuest')
    Main()