from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
from PyQt5.uic import *

import traceback, sys
import os
import mainwindow
import random
import csv
import resources
from playsound import playsound
import pygame

# This project is an fbs project. Additional dependencies are playsound, 
# PyQt5 == 5.9.2, pyobjc (macOS only) and pygame.

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.ui = mainwindow.Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

        # Setup events for labels
        for i in self.ui.labels:
            for x in i:
                x.installEventFilter(self)

        # Connect Buy/Sell Buttons
        self.ui.m_item1buy.clicked.connect(lambda:self.buyItem(0))
        self.ui.m_item2buy.clicked.connect(lambda:self.buyItem(1))
        self.ui.m_item3buy.clicked.connect(lambda:self.buyItem(2))
        self.ui.m_item4buy.clicked.connect(lambda:self.buyItem(3))
        self.ui.m_item5buy.clicked.connect(lambda:self.buyItem(4))
        self.ui.m_item6buy.clicked.connect(lambda:self.buyItem(5))
        self.ui.m_item1sell.clicked.connect(lambda:self.sellItem(0))
        self.ui.m_item2sell.clicked.connect(lambda:self.sellItem(1))
        self.ui.m_item3sell.clicked.connect(lambda:self.sellItem(2))
        self.ui.m_item4sell.clicked.connect(lambda:self.sellItem(3))
        self.ui.m_item5sell.clicked.connect(lambda:self.sellItem(4))
        self.ui.m_item6sell.clicked.connect(lambda:self.sellItem(5))
        self.ui.i_sellButton.clicked.connect(lambda:self.sellMB())

        # Set up countries
        self.countries = []
        self.unlocked = []
        e = random.Random()
        self.country = e.randint(0,5)
        print(self.country)
        self.randomCount = e.randint(0,10)
        self.counting = 0
        self.wait = 6

        # Base Prices
        self.bp1 = 1
        self.bp2 = 5
        self.bp3 = 3
        self.bp4 = 10
        self.bp5 = 5
        self.bp6 = 1

        # Set up prices
        self.p1 = 1
        self.p2 = 5
        self.p3 = 3
        self.p4 = 10
        self.p5 = 5
        self.p6 = 1
        self.prices = [self.p1, self.p2, self.p3, self.p4, self.p5, self.p6]

        # Set up Inventory
        self.i1 = 0
        self.i2 = 0
        self.i3 = 0
        self.i4 = 0
        self.i5 = 0
        self.i6 = 0
        self.inventory = [self.i1, self.i2, self.i3, self.i4, self.i5, self.i6]
        self.unitMP = [5000, 5000, 10000, 10, 2, 10] # Item unit multipliers

        # Set up bank
        self.warned = False
        self.praised = False
        self.interest = 0.012
        self.storedMoney = 0
        self.debt = 0
        self.borrowLimit = 200
        self.ui.b_borrowButton.clicked.connect(lambda:self.borrowMoney())
        self.ui.b_repayButton.clicked.connect(lambda:self.repayMoney())
        self.ui.b_depositButton.clicked.connect(lambda:self.depositMoney())
        self.ui.b_withdrawButton.clicked.connect(lambda:self.withdrawMoney())
        
        # Unit Requirements:
        self.unitReq = [50, 1000, 200, 1, 1 ,1]

        self.unitPrice = 10

        self.unit = 0
        self.money = 50

        # Set up bank
        self.loans = []

        # Set up sounds - Component 3
        self.sounds = True
        pygame.mixer.pre_init(44100, -16, 2, 4096)
        pygame.init()
        pygame.mixer.init()
        
        self.music = pygame.mixer.Sound("./src/main/python/ppp.wav")
        self.music.play(-1)
        playsound("./src/main/python/bell-peters.wav", False)

        c = open("src/main/python/countries.txt")
        for l in c:
            line = l.split("|")
            country = Country(
                int(line[0]), line[1],
                float(line[2]), float(line[3]),
                float(line[4]), float(line[5]),
                float(line[6]), float(line[7])
            )

            self.countries.append(country)

        print(self.countries) # It's ugly, but game sometimes throws index error without this for some reason.

    def eventFilter(self, obj, event):
        # Check for clicks
        if event.type() == QEvent.MouseButtonRelease:

            if obj in self.ui.mm:
                if obj == self.ui.mm[0]:
                    self.setStyleSheet("")
                    self.loadCountry(self.country)
                    self.refreshGame()
                    self.ui.StackedWidget.setCurrentIndex(1)
                # Royalty free music used is Pixel Peeker Polka by Kevin MacLeod. 
                # Bell sound used with permission from Peter Howse.
                elif obj == self.ui.mm[2]:
                    qm = QMessageBox
                    a = qm.question(self,'', "Enable Music and Sounds?", qm.Yes | qm.No)
                    if a == qm.Yes:
                        if not self.sounds:
                            self.sounds = True
                            self.music.play(-1)
                    elif a == qm.No:
                        self.sounds = False
                        self.music.stop()

                elif obj == self.ui.mm[3]:
                    sys.exit()
                else:
                    self.loadGame()

            elif obj in self.ui.m:
                if obj == self.ui.m[0]:
                    self.loadBank()
                elif obj == self.ui.m[1]:
                    self.loadInventory()
                elif obj == self.ui.m[2]:
                    self.loadMap()
                elif obj == self.ui.m[3]:
                    self.saveGame()
                elif obj == self.ui.m[4]:
                    qm = QMessageBox
                    a = qm.question(self,'', "Enable Music and Sounds?", qm.Yes | qm.No)
                    if a == qm.Yes:
                        if not self.sounds:
                            self.sounds = True
                            self.music.play(-1)
                    elif a == qm.No:
                        self.sounds = False
                        self.music.stop()
                elif obj == self.ui.m[5]:
                    sys.exit()
            
            elif obj in self.ui.b:
                if obj == self.ui.b[0]:
                    self.setStyleSheet("")
                    self.ui.StackedWidget.setCurrentIndex(1)
                elif obj == self.ui.b[1]:
                    self.loadInventory()
                elif obj == self.ui.b[2]:
                    self.loadMap()
                elif obj == self.ui.b[3]:
                    self.saveGame()
                elif obj == self.ui.b[4]:
                    qm = QMessageBox
                    a = qm.question(self,'', "Enable Music and Sounds?", qm.Yes | qm.No)
                    if a == qm.Yes:
                        if not self.sounds:
                            self.sounds = True
                            self.music.play(-1)
                    elif a == qm.No:
                        self.sounds = False
                        self.music.stop()
                elif obj == self.ui.b[5]:
                    sys.exit()

            elif obj in self.ui.i:
                if obj == self.ui.i[0]:
                    self.loadBank()
                elif obj == self.ui.i[1]:
                    self.setStyleSheet("")
                    self.ui.StackedWidget.setCurrentIndex(1)
                elif obj == self.ui.i[2]:
                    self.loadMap()
                elif obj == self.ui.i[3]:
                    self.saveGame()
                elif obj == self.ui.i[4]:
                    qm = QMessageBox
                    a = qm.question(self,'', "Enable Music and Sounds?", qm.Yes | qm.No)
                    if not self.sounds:
                            self.sounds = True
                            self.music.play(-1)
                    elif a == qm.No:
                        self.sounds = False
                        self.music.stop()
                elif obj == self.ui.i[5]:
                    sys.exit()

            # Country Unlocks. Each loc has unique pricing, and cheapest trade route is not in
            # number order. Could be managed into a for loop or something rather than so many
            # if statements, but it works for the time being.
            elif obj in self.ui.w_loc:
                if obj == self.ui.w_loc[0]:
                    if self.countries[0] in self.unlocked:
                        self.loadCountry(0)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 200:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock China for $200k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 200
                            self.unlocked.append(self.countries[0])
                            self.loadCountry(0)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()
                
                elif obj == self.ui.w_loc[1]:
                    if self.countries[1] in self.unlocked:
                        self.loadCountry(1)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 500:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock Japan for $500k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 500
                            self.unlocked.append(self.countries[1])
                            self.loadCountry(1)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()

                elif obj == self.ui.w_loc[2]:
                    if self.countries[2] in self.unlocked:
                        self.loadCountry(2)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 1000:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock USA for $1000k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 1000
                            self.unlocked.append(self.countries[2])
                            self.loadCountry(2)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()
                
                elif obj == self.ui.w_loc[3]:
                    if self.countries[3] in self.unlocked:
                        self.loadCountry(3)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 2000:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock Australia for $2000k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 2000
                            self.unlocked.append(self.countries[3])
                            self.loadCountry(3)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()

                elif obj == self.ui.w_loc[4]:
                    if self.countries[4] in self.unlocked:
                        self.loadCountry(4)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 300:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock Vietnam for $300k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 300
                            self.unlocked.append(self.countries[4])
                            self.loadCountry(4)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()

                elif obj == self.ui.w_loc[5]:
                    if self.countries[5] in self.unlocked:
                        self.loadCountry(5)
                        self.ui.StackedWidget.setCurrentIndex(1)
                    elif self.money >= 1200:
                        qm = QMessageBox
                        a = qm.question(self,'', "Unlock Costa Rica for $1200k?", qm.Yes | qm.No)
                        if a == qm.Yes:
                            self.money -= 1200
                            self.unlocked.append(self.countries[5])
                            self.loadCountry(5)
                            self.ui.StackedWidget.setCurrentIndex(1)
                            self.refreshGame()
                            self.dingDong()
                    else:
                        msgBox = QMessageBox()
                        msgBox.setText("You do not have enough money to unlock this country.")
                        msgBox.exec()

        return False

    def resetPrices(self):
        u = self.ui.costs
        r = random.Random()
        n = self.country
        # Cost = Base * Multiplier + RNG
        self.p1 = self.bp1 * self.countries[n].mp1 + r.randint(0, 5)
        self.p2 = self.bp2 * self.countries[n].mp2 + r.randint(-1, 2)
        self.p3 = self.bp3 * self.countries[n].mp3 + r.randint(0, 3)
        self.p4 = self.bp4 * self.countries[n].mp4 + r.randint(-2, 4)
        self.p5 = self.bp5 * self.countries[n].mp5 + r.randint(0, 1)
        self.p6 = self.bp6 * self.countries[n].mp6 + r.randint(0, 3)
        self.prices = [int(self.p1), int(self.p2), int(self.p3), int(self.p4), int(self.p5), int(self.p6)]

        if self.wait > 5:
            self.unitPrice = 1 * random.Random().randint(10, 50)

        # RNG Event
        if random.Random().randint(0, 1000) >= 997:
            self.wait = 0
            self.unitPrice = 100
            self.dingDong()
            msgBox = QMessageBox()
            msgBox.setText("New CPU platform is out! Motherboard Prices are up for a limited time!")
            msgBox.exec()
        
        # RNG Event - Stock Market Crash
        if random.Random().randint(1001, 2000) >= 1995:
            self.storedMoney = 0
            self.dingDong()
            msgBox = QMessageBox()
            msgBox.setText("Oh no! The stock market has crashed! Any money you've stored now is gone.")
            msgBox.exec()

        if random.Random().randint(2001, 3000) <= 2010:
            self.money -= round(self.money/2.5)
            self.dingDong()
            msgBox = QMessageBox()
            msgBox.setText("Oh no! You were investigated for tax fraud. You have been fined accordingly.")
            msgBox.exec()

        # Update Price Labels
        for i in range(6):
            text = "$" + str(self.prices[i]) + "k" 
            u[i].setText(text)

        self.wait += 1

    def loadCountry(self, country):
        self.setStyleSheet("")
        self.country = country
        self.resetPrices()
        if self.countries[self.country] not in self.unlocked:
            self.unlocked.append(self.countries[self.country])
        for i in self.unlocked:
            print(i.name, end=" ")
        u = self.ui
        text = self.countries[self.country].name
        u.m_title.setText(text)

    def refreshGame(self):
        u = self.ui

        if self.counting >= self.randomCount:
            self.resetPrices()
            self.randomCount = random.Random().randint(0,10)
            self.debt += round(self.debt * self.interest)
            self.storedMoney += round(self.storedMoney * (self.interest/2))
            self.dingDong()
            self.counting = 0

        # Refresh Cash
        for i in u.moneyInd:
            i.setText("$" + str(self.money) + "k")
        
        #Refresh Units
        for i in u.mbUnits:
            i.setText(str(self.unit))

        u.i_sellPrice.setText("Current Selling Price: $" + str(self.unitPrice) + "k")

        # Refresh Inventory
        for i in range(6):
            if self.inventory[i] < 1000:
                u.m_inv[i].setText("In Inventory: " + str(self.inventory[i]))
            else:
                u.m_inv[i].setText("In Inventory: " + str(self.inventory[i]/1000) + "k")

        # Refresh finances
        self.interest = random.Random().randint(10, 30)/1000

        bl = (self.money * 4) - (self.debt * 5)
        if bl >= 0:
            self.borrowLimit = bl
        else:
            self.borrowLimit = 0

        u.b_debt.setText("$" + str(self.debt) + "k")
        u.b_limit.setText("Borrow Limit: $" + str(self.borrowLimit) + "k")
        u.b_interestRate.setText("Current Interest: " + str(round(self.interest*100)) + "%")
        u.b_storedMoney.setText("$" + str(self.storedMoney) + "k")
        
        if self.debt >= 1500 and not self.warned:
            msgBox = QMessageBox()
            msgBox.setText("Warning! You have accumulated a lot of debt! If you keep going, you will approach bankruptcy!")
            msgBox.exec()
            self.warned = True

        elif self.debt >= 2000:
            msgBox = QMessageBox()
            msgBox.setText("You have too much debt! You are now bankrupt. Game Over.")
            msgBox.exec()
            sys.exit()

        if self.unit >= 700000 and not self.praised:
            msgBox = QMessageBox()
            msgBox.setText("Almost there! Only " + str(1000000-self.unit) + " units more until you can comfortably retire!")
            msgBox.exec()
            self.praised = True

        l = 0
        for i in self.inventory:
            l += i

        if self.money <= 0 and self.borrowLimit <= 0 and self.storedMoney == 0 and self.debt >= 0 and l == 0:
            msgBox = QMessageBox()
            msgBox.setText("You have no money left, and no means of borrowing more! Game Over.")
            msgBox.exec()
            sys.exit()

        u.i_i1.setText("Capacitors x " + str(self.inventory[0]))
        u.i_i2.setText("Plastic Slots and Casing x " + str(self.inventory[1]))
        u.i_i3.setText("Chokes and MOSFETs x " + str(self.inventory[2]))
        u.i_i4.setText("Sockets x " + str(self.inventory[3]))
        u.i_i5.setText("Chipsets x " + str(self.inventory[4]))
        u.i_i6.setText("PCB x " + str(self.inventory[5]))

    def loadBank(self):
        self.setStyleSheet("")
        self.ui.StackedWidget.setCurrentIndex(3)

    def loadInventory(self):
        self.setStyleSheet("")
        self.ui.StackedWidget.setCurrentIndex(4)

    def loadMap(self):
        self.ui.StackedWidget.setCurrentIndex(2)
    
    # Custom feature 1 - Save games
    def saveGame(self):
        f, ok = QFileDialog.getSaveFileName(self, "Select Save File Location", "~/PyTrade/save.pp", "Saves (*.pp)")
        print(f)
        if ok:
            try:
                ul = []
                for i in self.unlocked:
                    ul.append(i.name)
                with open(f, "w") as ff:
                    wr = csv.writer(ff)
                    l = [self.country, ul, self.money, self.unit, self.inventory, self.debt, self.storedMoney]
                    wr.writerow(l)
                msgBox = QMessageBox()
                msgBox.setText(f"Success! File saved to {f}")
                msgBox.exec()
            except:
                msgBox = QMessageBox()
                msgBox.setText("Error writing file. Please check file permissions.")
                msgBox.exec()
    
    def loadGame(self):
        f, ok = QFileDialog.getOpenFileName(self, "Select Save File Location", "~/PyTrade/save.pp", "Saves (*.pp)")
        if ok:
            try: 
                with open(f, "r") as ff:
                    r = csv.reader(ff)
                    l = list(r)
                    self.country = int(l[0][0])
                    # Converts the string list into real list, and then compares the names to add the country to the
                    # self.unlocked list. 
                    # Reference: https://www.geeksforgeeks.org/python-convert-a-string-representation-of-list-into-list/
                    for i in self.countries:
                        if i.name in l[0][1].strip('][').split(', '):
                            self.unlocked.append(i)
                    self.money = round(float(l[0][2]))
                    self.unit = int(l[0][3])
                    # Converts string list to real list, and then converts its contents to int.
                    self.inventory = l[0][4].strip('][').split(', ')
                    for i in range(6):
                        self.inventory[i]
                        self.inventory[i] = int(self.inventory[i])
                    self.debt = round(float(l[0][5]))
                    self.storedMoney = round(float(l[0][6]))
                msgBox = QMessageBox()
                msgBox.setText("Success! Save loaded.")
                msgBox.exec()
                self.refreshGame()
                self.loadCountry(self.country)
                self.ui.StackedWidget.setCurrentIndex(1)
            except:
                msgBox = QMessageBox()
                msgBox.setText("Error reading file.")
                msgBox.exec()

    def buyItem(self, i):
        if self.money >= self.prices[i]:
            self.money -= self.prices[i]
            self.inventory[i] += self.unitMP[i]
            self.counting += 1
            self.refreshGame()
        else:
            msgBox = QMessageBox()
            msgBox.setText("Not enough money!")
            msgBox.exec()

    def sellItem(self, i):
        if self.inventory[i] >= self.unitMP[i]:
            self.money += self.prices[i]
            self.inventory[i] -= self.unitMP[i]
            self.counting += 1
            self.refreshGame()
        elif self.inventory[i] > 0:
            self.money += round(self.prices[i] * (self.inventory[i]/self.unitMP[i]))
            self.inventory[i] = 0
            self.counting += 1
            self.refreshGame()
        else:
            msgBox = QMessageBox()
            msgBox.setText("You do not possess enough units of this item to sell.")
            msgBox.exec()

    # Custom feature 2 - Own product to sell
    def sellMB(self):
        for i in range(6):
            if self.inventory[i] < self.unitReq[i]:
                msgBox = QMessageBox()
                msgBox.setText("You do not possess enough materials to manufacture motherboards.")
                msgBox.exec()
                break
            else:
                amt, ok = QInputDialog.getText(self, "Sell Motherboards", "Enter amount of units to sell:")
                    
                if ok:
                    try:
                        n = int(amt)
                        validity = [1, 1, 1, 1, 1, 1]
                        for i in range(6):
                            if self.inventory[i] < self.unitReq[i] * n:
                                validity[i] -= 1

                        if 0 not in validity:
                            for i in range(6):
                                self.inventory[i] -= self.unitReq[i] * n
                            self.money += self.unitPrice * n
                            self.unit += n
                            self.resetPrices()
                            self.refreshGame()
                        else:
                            msgBox = QMessageBox()
                            msgBox.setText("You do not possess enough materials to manufacture this many motherboards.")
                            msgBox.exec()
                    except:
                        msgBox = QMessageBox()
                        msgBox.setText("Invalid Input")
                        msgBox.exec()
                break

    def borrowMoney(self):
        m, ok = QInputDialog.getText(self, "Borrow Money", "Enter amount to borrow (in $k):")

        if ok:
            try:
                n = round(float(m))

                if n <= self.borrowLimit:
                    self.money += n
                    self.debt += n
                    self.refreshGame()
                elif n > self.borrowLimit:
                    msgBox = QMessageBox()
                    msgBox.setText("You cannot borrow that much money.")
                    msgBox.exec()
            except:
                msgBox = QMessageBox()
                msgBox.setText("Invalid Input")
                msgBox.exec()

    def repayMoney(self):
        m, ok = QInputDialog.getText(self, "Repay Money", "Enter amount to repay (in $k):")
        if ok:
            try:
                n = float(m)

                if n <= self.debt:
                    if n > self.money:
                        msgBox = QMessageBox()
                        msgBox.setText("You don't have that much money.")
                        msgBox.exec()
                    else:
                        self.debt -= n
                        self.money -= n
                        self.refreshGame()
                elif n > self.debt:
                    msgBox = QMessageBox()
                    msgBox.setText("You don't have that much debt.")
                    msgBox.exec()
            except:
                msgBox = QMessageBox()
                msgBox.setText("Invalid Input")
                msgBox.exec()

    def withdrawMoney(self):
        m, ok = QInputDialog.getText(self, "Withdraw Money", "Enter amount to withdraw (in $k):")
        if ok:
            try:
                n = float(m)

                if n <= self.storedMoney:
                    self.storedMoney -= n
                    self.money += n
                    self.refreshGame()
                elif n > self.storedMoney:
                    msgBox = QMessageBox()
                    msgBox.setText("You don't have that much money stored.")
                    msgBox.exec()
            except:
                msgBox = QMessageBox()
                msgBox.setText("Invalid Input")
                msgBox.exec()

    def depositMoney(self):
        m, ok = QInputDialog.getText(self, "Deposit Money", "Enter amount to deposit (in $k):")
        if ok:
            try:
                n = round(float(m))

                
                if n > self.money:
                    msgBox = QMessageBox()
                    msgBox.setText("You don't have that much money.")
                    msgBox.exec()
                else:
                    self.storedMoney += n
                    self.money -= n
                    self.refreshGame()
            except:
                msgBox = QMessageBox()
                msgBox.setText("Invalid Input")
                msgBox.exec()

    def dingDong(self):
        if self.sounds:
            playsound("./src/main/python/bell-peters.wav", False)

class Country:
    def __init__(self, index, name, mp1, mp2, mp3, mp4, mp5, mp6):
        self.index = index
        self.name = name
        self.mp1 = mp1
        self.mp2 = mp2
        self.mp3 = mp3
        self.mp4 = mp4
        self.mp5 = mp5
        self.mp6 = mp6

# class Loan:
    # def __init__(self, ):  todo

# Generated by fbs
if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    window = MainWindow()
    window.resize(1280, 720)
    window.show()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)