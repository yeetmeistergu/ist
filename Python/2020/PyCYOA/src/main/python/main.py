from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *

import sys
import os
import cyoa_ui
import random

class AppWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(AppWindow, self).__init__(*args, **kwargs)
        
        # Setting up variables
        self.rooms = []
        self.room_id = 0
        self.challenged = False

        # Window setup
        self.ui = cyoa_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("CYOA")

        f = open("src/main/python/story.txt")

        # read in each line of the file
        for l in f:

            # split the file on the pipes "|"
            line = l.split("|")

            # create a new instance of the Room class
            room = Room(
                int(line[0]), line[1],
                int(line[2]), line[3],
                int(line[4]), line[5],
                int(line[6]), line[7])

            # add the room to the array of rooms
            self.rooms.append(room)
        
        # Setting up window commands
        self.ui.actionRestart.triggered.connect(lambda:self.load_room(0))
        self.ui.actionExit.triggered.connect(sys.exit)

        # Set up option buttons
        self.ui.buttonA.clicked.connect(lambda:self.load_rooms(1))
        self.ui.buttonB.clicked.connect(lambda:self.load_rooms(2))
        self.ui.buttonC.clicked.connect(lambda:self.load_rooms(3))

        self.load_room(0)
        

    def load_room(self, room_id):
        rooms = self.rooms
        self.room_id = room_id
        self.ui.label.setText(QCoreApplication.translate("MainWindow", rooms[room_id].story))

        r = random.Random().randint(0,1)

        if r == 0:
            self.setStyleSheet("QWidget{background-image: url(:/images/97725660-luxury-white-background-with-painted-gray-abstract-marble-texture-in-elegant-fancy-design.jpg);}")
        # elif r == 1:
        #     self.setStyleSheet("QWidget{background-image: url(:/images/banner.png);}")
        else:
            self.setStyleSheet("QWidget{background-image: url(:/images/father jablonski.jpg);}")

        # Sets the buttons to the available options.
        if rooms[room_id].opt1_txt.strip() != "-":
            self.ui.buttonA.setText(rooms[room_id].opt1_txt)
            self.ui.buttonA.show()
        else:
            self.ui.buttonA.setText("")
            self.ui.buttonA.hide()
        
        if rooms[room_id].opt2_txt.strip() != "-":
            self.ui.buttonB.setText(rooms[room_id].opt2_txt) 
            self.ui.buttonB.show()
            
        else:
            self.ui.buttonB.setText("")
            self.ui.buttonB.hide()

        if rooms[room_id].opt3_txt.strip() != "-":
            self.ui.buttonC.setText(rooms[room_id].opt3_txt)
            self.ui.buttonC.show()
            
        else:
            self.ui.buttonC.setText("")
            self.ui.buttonC.hide()

    # Dialog box will infinite loop without this for some reason and I cannot figure out why. 
    # Might have been a logic error with the connecting the buttons or something but this works
    # and I don't want to risk breaking it again.
    def load_rooms(self, r):
        if r == 1:
            self.load_room(self.rooms[self.room_id].opt1_id)
        elif r == 2:
            self.load_room(self.rooms[self.room_id].opt2_id)
        elif r == 3:
            self.load_room(self.rooms[self.room_id].opt3_id)
        if self.room_id == 12:
            self.load_challenge(r)
        if self.room_id > 0 and self.room_id < 12:
            self.challenged = False

    def load_challenge(self, r):
        # Without this variable, room_id always resets to 12 for some odd reason.
        if not self.challenged:
            print(r)
            text, ok = QInputDialog.getText(self, 'Insult the 12 year old', 'Enter your Insult:')
            
            if ok:
                if "mum" in text or "mamma" in text or "mom" in text or "kill yourself" in text or "pp" in text or "smol" in text or "retard" in text:
                    self.load_room(13)
                else:
                    self.load_room(14)
            else:
                self.load_room(14)
            self.challenged = True

class Room:

    # this section sets up what happens when we create a new room
    def __init__(
        self, id, story, opt1_id, opt1_txt,
        opt2_id, opt2_txt, opt3_id, opt3_txt):

        self.id = id
        self.story = story
        self.opt1_id = opt1_id
        self.opt1_txt = opt1_txt
        self.opt2_id = opt2_id
        self.opt2_txt = opt2_txt
        self.opt3_id = opt3_id
        self.opt3_txt = opt3_txt

if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    window = AppWindow()
    window.resize(600, 400)
    window.show()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)