import sys
import platform
import time
import main
from os import system
from random import choice
from math import inf as infinity

# Doesn't work
# An implementation of Minimax AI Algorithm in Connect 4 using Python
# This software is available under GPL license.
# Adapted from https://github.com/Cledersonbc/tic-tac-toe-minimax/blob/master/py_version/minimax.py
# Author of original code: Clederson Cruz
# Year: 2017
# Adapted for this application by: Louie Gu
# Year: 2020
# License: GNU GENERAL PUBLIC LICENSE (GPL)

MAX = +1
MIN = -1

def evaluate(state):
    """
    Function for heuristic evaluation of state.
    :state: the state of the current board
    :return: +1 if the computer wins; -1 if the human wins; 0 draw
    """
    if wins(state, MIN):
        score = +1
    elif wins(state, MAX):
        score = -1
    else:
        score = 0

    return score

def wins(state, player):
    """
    This function tests if a specific player wins. Possibilities:
    * Three rows    [X X X] or [O O O]
    * Three cols    [X X X] or [O O O]
    * Two diagonals [X X X] or [O O O]
    :state: the state of the current board
    :player: a human or a computer
    :return: True if the player wins
    """
    moveset = []
    winner = None
    # Check Horizontal
    for i in state:
        for j in range(4):
            if i[j] == i[j+1] == i[j+2] == i[j+3]:
                if i[j] == 1:
                    winner = +1
                elif i[j] == -1:
                    winner = -1

    # Check Vertical
    for i in range(7): # for each collumn
        moveset = []
        # append the character in collumn e of every row
        for e in state:
            moveset.append(e[i])
        for n in range(3):
            if moveset[n] == moveset[n+1] == moveset[n+2] == moveset[n+3]:
                if moveset[n] == 1:
                    winner = +1
                elif moveset[n] == -1:
                    winner = -1

    # Check Diagonal from left
    for i in range(3):
        for j in range(4):
            if state[i][j] == state[i+1][j+1] == state[i+2][j+2] == state[i+3][j+3]:
                if state[i][j] == 1:
                    winner = +1
                elif state[i][j] == -1:
                    winner = -1
    
    # Check Diagonal from right
    for i in range(3):
        for j in range(0, 4):
            if state[i][6-j] == state[i+1][5-j] == state[i+2][4-j] == state[i+3][3-j]:
                if state[i][6-j] == 1:
                    winner = +1
                elif state[i][6-j] == -1:
                    winner = -1
    
    if winner == player:
        return True
    else:
        return False

def game_over(state):
    """
    This function tests if the human or computer wins
    :state: the state of the current board
    :return: True if the human or computer wins
    """
    return wins(state, MAX) or wins(state, MIN)

def empty_cells(state):
    """
    Each empty cell will be added into cells list
    :state: the state of the current board
    :return: list of empty cells
    """
    cells = []

    # For every row, check the cell in every collumn.
    for x in range(7):
        for y in range(6):
            if state[y][x] == 0:
                cells.append([x, y])
                break

    print(cells)

    return cells

def minimax(state, depth, player, alpha, beta):
    """
    AI function that chooses the best move
    :state: current state of the board
    :depth: node index in the tree
    :player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
    if player == MIN:
        best = [-1, -1, -infinity]
    else:
        best = [-1, -1, +infinity]

    # For the recursion, returns a score for each move combination.
    if depth == 42 or game_over(state) or empty_cells(state) == []:
        score = evaluate(state)
        return [-1, -1, score]

    for cell in empty_cells(state):
        x, y = cell[0], cell[1]

        # We make every possible move from this state, and then see the outcome.
        state[y][x] = player

        # Recursion, plays every possible move from this situation and obtains the outcome for all the options.
        score = minimax(state, depth + 1, -player, alpha, beta) 
        if player == MIN:
            best = [-1, -1, max(best[2], player)]  
            alpha = max(alpha, best[2])  
        else:
            best = [+1, +1, max(best[2], player)]
            beta = max(beta, best[2])
  
        # Alpha Beta Pruning  
        if beta <= alpha:  
            break 
        
        # We reset the cell after the calculations.
        state[y][x] = 0
        # We give the score to the cell being tested.
        score[0], score[1] = x, y

        if player == MIN:
            # We give player the better score.
            if score[2] > best[2]:
                best = score

        else:
            # We keep the good score for the player.
            if score[2] < best[2]:
                best = score 

    return best
