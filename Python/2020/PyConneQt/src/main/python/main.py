# This Python file uses the following encoding: utf-8
import sys
import random
# import minimax
import game_ui
from math import inf
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *

class Game(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)

        # Set Up Window
        self.ui = game_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("PyconneQt")

        # Connect global menu buttons
        self.ui.actionNew_Game.triggered.connect(self.newGame)
        self.ui.actionExit_Game.triggered.connect(sys.exit)

        # Install label events
        for i in self.ui.labels:
            i.installEventFilter(self)

        # Set up grid
        r5 = [0, 0, 0, 0, 0, 0, 0]
        r4 = [0, 0, 0, 0, 0, 0, 0]
        r3 = [0, 0, 0, 0, 0, 0, 0]
        r2 = [0, 0, 0, 0, 0, 0, 0]
        r1 = [0, 0, 0, 0, 0, 0, 0]
        r0 = [0, 0, 0, 0, 0, 0, 0]
        self.grid = [r0, r1, r2, r3, r4, r5]
        self.bigBoard = []
        self.updateBoard()

        # Set up game characters
        self.turn = 1
        self.taken = 0
        self.winner = 0
        
        # Set game state
        self.mode = 0
        self.game = False
    
    def newGame(self):

        r5 = [0, 0, 0, 0, 0, 0, 0]
        r4 = [0, 0, 0, 0, 0, 0, 0]
        r3 = [0, 0, 0, 0, 0, 0, 0]
        r2 = [0, 0, 0, 0, 0, 0, 0]
        r1 = [0, 0, 0, 0, 0, 0, 0]
        r0 = [0, 0, 0, 0, 0, 0, 0]
        self.grid = [r0, r1, r2, r3, r4, r5]
        self.bigBoard = []
        self.updateBoard()

        self.turn = 1
        self.taken = 0
        self.winner = 0

        # Ask if 1 player or 2 player
        p = QMessageBox()
        p.setText("Select game mode:")
        p.setIcon(QMessageBox.Information)
        op = QPushButton("One Player")
        p.addButton(op, QMessageBox.NoRole)
        tp = QPushButton("Two Player")
        p.addButton(tp, QMessageBox.YesRole)
        p.setDefaultButton(op)
        p.exec_()
        option = p.clickedButton().text()

        if option == "One Player":
            self.mode = 0
        else:
            self.mode = 1

        self.game = True

    def eventFilter(self, obj, event):
        # Check for clicks
        if event.type() == QEvent.MouseButtonRelease:

            # Only activate if game in progress
            if self.game == True:
                for i in range(7):
                    if obj == self.ui.labels[i]:
                        self.checkMove(i)
                        return True

        return False

    def checkMove(self, i):
        valid = False
        if self.winner == 0:
            # Check if there is empty slot in the collumn
            for cell in self.grid:
                if cell[i] == 0:
                    valid = True
                if valid:
                    cell[i] = self.turn
                    self.updateBoard()
                    self.repaint()
                    self.update()
                    break

            if valid:
                self.checkWin()
                if self.turn == 1:
                    self.turn = -1

                    if self.mode == 0:
                        self.aiMove()

                elif self.turn == -1:
                    self.turn = 1

                # print(self.grid)
                self.taken += 1
                return True
            
            else:
                e = QMessageBox()
                e.setText("Invalid move!")
                e.setIcon(QMessageBox.Warning)
                e.exec_()
            return False

    def aiMove(self):
        if self.winner == 0:
            # minimax.empty_cells(self.grid) -- Algorithm doesnt seem to work :(
            # m = minimax.minimax(self.grid, self.taken, self.turn, -inf, +inf)[0]
            m = random.Random().randint(0,6)
            self.checkMove(m)
            # return True
        # return False

    def checkWin(self):
        moveset = []
        # Check Horizontal
        for i in self.grid:
            for j in range(4):
                if i[j] == i[j+1] == i[j+2] == i[j+3]:
                    if i[j] == 1:
                        self.winner = 1
                    elif i[j] == -1:
                        self.winner = -1

        # Check Vertical
        for i in range(7): # for each collumn
            moveset = []
            # append the character in collumn e of every row
            for e in self.grid:
                moveset.append(e[i])
            for n in range(3):
                if moveset[n] == moveset[n+1] == moveset[n+2] == moveset[n+3]:
                    if moveset[n] == 1:
                        self.winner = 1
                    elif moveset[n] == -1:
                        self.winner = -1

        # Check Diagonal from left
        for i in range(3):
            for j in range(4):
                if self.grid[i][j] == self.grid[i+1][j+1] == self.grid[i+2][j+2] == self.grid[i+3][j+3]:
                    if self.grid[i][j] == 1:
                        self.winner = 1
                    elif self.grid[i][j] == -1:
                        self.winner = -1
        
        # Check Diagonal from right
        for i in range(3):
            for j in range(0, 4):
                if self.grid[i][6-j] == self.grid[i+1][5-j] == self.grid[i+2][4-j] == self.grid[i+3][3-j]:
                    if self.grid[i][6-j] == 1:
                        self.winner = 1
                    elif self.grid[i][6-j] == -1:
                        self.winner = -1

        if self.winner != 0:
            print("Winner:", self.winner)
                  
            if self.winner == 1:
                msg = "Red wins!  Play again?"
            elif self.winner == -1:
                msg = "Red wins!  Play again?"

            # If there's a draw, handle it here
            if self.winner == "" and self.taken == 42:
                msg = "It's a draw!  Play again?"

            choice = QMessageBox()
            choice.setText(msg)
            choice.setIcon(QMessageBox.Information)
            yes = QPushButton("Yes")
            no = QPushButton("No")
            choice.addButton(yes, QMessageBox.NoRole)
            choice.addButton(no, QMessageBox.YesRole)
            choice.setDefaultButton(yes)
            choice.exec_()
            c = choice.clickedButton().text()

            if c == "Yes":
                self.newGame()
                
            elif c == "No":
                sys.exit()

            return True

        return False

    def paintEvent(self, event):
        
        qp = QPainter()
        qp.begin(self)

        # Draw Board
        qp.setPen(QPen(Qt.blue,  1, Qt.SolidLine))
        qp.setBrush(QBrush(Qt.blue, Qt.SolidPattern))
        qp.drawRect(0, 0, 700, 600)

        # Draw slots/tokens
        x = 5
        y = 505

        for i in range(42):
            if self.bigBoard[i] == 1:
                qp.setPen(QPen(Qt.red, 1, Qt.SolidLine))
                qp.setBrush(QBrush(Qt.red, Qt.SolidPattern))
            elif self.bigBoard[i] == -1:
                qp.setPen(QPen(Qt.yellow, 1, Qt.SolidLine))
                qp.setBrush(QBrush(Qt.yellow, Qt.SolidPattern))
            else:
                qp.setPen(QPen(Qt.white, 1, Qt.SolidLine))
                qp.setBrush(QBrush(Qt.white, Qt.SolidPattern))
                
            qp.drawEllipse(x, y, 90, 90)
            # print(x,y)
            if x < 600:
                x += 100
            else:
                x = 5
                y -= 100

        qp.end()
    
    def updateBoard(self):
        self.bigBoard = []
        for i in self.grid:
            for e in i:
                self.bigBoard.append(e)
        # print(self.bigBoard)
        self.repaint()

if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    window = Game()
    window.show()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)