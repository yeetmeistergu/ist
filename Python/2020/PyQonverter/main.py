# from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *

import sys
import os
import mainwindow

# Few issues which cannot be fixed at the moment:
# Folders with special characters break on windows
# Files with spaces break on windows

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.ui = mainwindow.Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

        self.directory = ""
        self.queue = []
        self.format = []
        self.custom = []
        self.oname = []

        # Use system ffmpeg binaries if possible.
        self.haveffmpeg = True

        if os.system("ffmpeg -h") != 0:
            self.haveffmpeg = False
            msgBox = QMessageBox()
            msgBox.setText("Warning! ffmpeg is not installed in your system, or cannot be accessed. If this script is not run from its root directory, it will not be able to access the included ffmpeg binarys, and thus will not work.")
            msgBox.exec()

        self.ui.selFiles.clicked.connect(lambda:self.selectFiles())
        self.ui.selFormat.clicked.connect(lambda:self.selectFormat())
        self.ui.convertW.clicked.connect(lambda:self.convert("w"))
        self.ui.convertM.clicked.connect(lambda:self.convert("m"))

    def selectFiles(self):
        # Folders with special characters cause issues within the software.
        f = QFileDialog.getExistingDirectory(self, "Select directory to convert", "~/")
        try:
            self.directory = f
            print(self.directory)
            for i in os.listdir(self.directory):
                if i != ".DS_Store":
                    if " " in i:
                        msgBox = QMessageBox()
                        msgBox.setText("Error, please make sure files do not have spaces or certain special characters such as / or \\ ")
                        msgBox.exec()
                        break
                    self.queue.append(i)
                    self.custom.append("")
                    self.oname.append(i.rsplit(".", 1)[0])
                    self.format.append("")
                    m = QListWidgetItem(str(i), self.ui.qlist)
            self.ui.qlist.itemPressed.connect(lambda:self.manageFile())
        except:
            msgBox = QMessageBox()
            msgBox.setText("Error loading directory. Please check permissions.")
            msgBox.exec()
        # print(self.queue)
        # print(self.custom)
        # print(self.oname)

    def selectFormat(self):
        f, ok = QInputDialog.getText(self, "Select Format", "Select File Format to convert to (i.e. '.mp3'):")
        if ok:
            self.format.clear()
            for i in self.queue:
                self.format.append(str(f))

    def convert(self, p):
        msgBox = QMessageBox()
        msgBox.setText("Queue Starting... Please refer to console for status updates.")
        msgBox.exec()

        # Windows Procedure
        if p == "w":
            for i in range(len(self.queue)):
                # check if output folder exists. if not, create it.
                c = f'dir "{self.directory}/converted"'
                print(c)
                if os.system(c) != 0:
                    mk = f'mkdir "{self.directory}/converted"'
                    print(mk)
                    os.system(mk)

                # could have the initial part as its own variable, but this works well enough.
                if self.haveffmpeg:
                    command = f'ffmpeg.exe -i "{self.directory}/{self.queue[0]}" {self.custom[0]} "{self.directory}/converted/{self.oname[0]}{self.format[0]}"'
                else:
                    command = f'cd ./ffmpeg/Windows/bin && ffmpeg.exe -i "{self.directory}/{self.queue[0]}" {self.custom[0]} "{self.directory}/converted/{self.oname[0]}{self.format[0]}"'
                print(command)

                if os.system(command) != 0:
                    msgBox = QMessageBox()
                    msgBox.setText("An error has occurred! Please check console for details.")
                    msgBox.exec()
                    sys.exit()
                else:
                    del(self.queue[0])
                    del(self.custom[0])
                    del(self.oname[0])
                    del(self.format[0])
                    self.ui.qlist.takeItem(0)

            msgBox = QMessageBox()
            msgBox.setText("Queue Complete!")
            msgBox.exec()

        # macOS Procedure
        elif p == "m":
            for i in range(len(self.queue)):
                # check if output folder exists. if not, create it.
                c = f'ls "{self.directory}/converted"'
                print(c)
                if os.system(c) != 0:
                    mk = f'mkdir "{self.directory}/converted"'
                    print(mk)
                    os.system(mk)

                if self.haveffmpeg:
                    command = f'ffmpeg -i "{self.directory}/{self.queue[0]}" {self.custom[0]} "{self.directory}/converted/{self.oname[0]}{self.format[0]}"'
                else:
                    command = f'./ffmpeg/macOS/ffmpeg -i "{self.directory}/{self.queue[0]}" {self.custom[0]} "{self.directory}/converted/{self.oname[0]}{self.format[0]}"'
                print(command)
                
                if os.system(command) != 0:
                    msgBox = QMessageBox()
                    msgBox.setText("An error has occurred! Please check console for details.")
                    msgBox.exec()
                    sys.exit()
                else:
                    del(self.queue[0])
                    del(self.custom[0])
                    del(self.oname[0])
                    del(self.format[0])
                    self.ui.qlist.takeItem(0)

            msgBox = QMessageBox()
            msgBox.setText("Queue Complete!")
            msgBox.exec()

    def manageFile(self):
        r = self.ui.qlist.currentRow()
        print(r)
        # Manage Selected File
        d = QMessageBox()
        d.setText("Manage Selection:")
        d.setIcon(QMessageBox.Information)
        easy = QPushButton("Cancel")
        normal = QPushButton("Remove from Queue")
        hard = QPushButton("Custom Argument")
        d.addButton(easy, QMessageBox.ActionRole)
        d.addButton(normal, QMessageBox.NoRole)
        d.addButton(hard, QMessageBox.YesRole)
        d.setDefaultButton(easy)
        d.exec_()
        diff = d.clickedButton().text()

        if diff == "Remove from Queue":
            del(self.queue[r])
            del(self.custom[r])
            print(self.queue)
            print(self.custom)
            self.ui.qlist.takeItem(r)
        elif diff == "Custom Argument":
            a, ok = QInputDialog.getText(self, "Custom Arguments", "Enter custom argument for this file:")
            self.custom[r] = str(a)
            b, ok = QInputDialog.getText(self, "Custom Format", "Enter custom format (leave blank to use default):")
            if ok:
                if b != "":
                    print(r)
                    self.format[r] = str(b)
                    print(self.format)
        else:
            pass


if __name__ == '__main__':

    # Remnants from fbs. No longer needed.
    # appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    # window = MainWindow()
    # window.resize(500, 500)
    # window.show()
    # exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    # sys.exit(exit_code)

    app = QApplication(sys.argv)
    window = MainWindow()
    window.resize(500, 500)
    window.resize
    #Add the close feature at the program with the X
    sys.exit(app.exec_())