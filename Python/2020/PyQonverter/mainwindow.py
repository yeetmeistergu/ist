# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\src\main\python\mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(500, 500)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(500, 500))
        MainWindow.setMaximumSize(QtCore.QSize(500, 500))
        font = QtGui.QFont()
        font.setFamily("Trebuchet MS")
        font.setPointSize(18)
        MainWindow.setFont(font)
        MainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.title = QtWidgets.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(10, 15, 481, 41))
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setObjectName("title")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 55, 481, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.qlist = QtWidgets.QListWidget(self.centralwidget)
        self.qlist.setGeometry(QtCore.QRect(30, 120, 440, 220))
        self.qlist.setObjectName("qlist")
        self.selFiles = QtWidgets.QPushButton(self.centralwidget)
        self.selFiles.setGeometry(QtCore.QRect(70, 355, 140, 45))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.selFiles.setFont(font)
        self.selFiles.setObjectName("selFiles")
        self.queue = QtWidgets.QLabel(self.centralwidget)
        self.queue.setGeometry(QtCore.QRect(10, 80, 481, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.queue.setFont(font)
        self.queue.setAlignment(QtCore.Qt.AlignCenter)
        self.queue.setObjectName("queue")
        self.selFormat = QtWidgets.QPushButton(self.centralwidget)
        self.selFormat.setGeometry(QtCore.QRect(290, 355, 140, 45))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.selFormat.setFont(font)
        self.selFormat.setObjectName("selFormat")
        self.convertW = QtWidgets.QPushButton(self.centralwidget)
        self.convertW.setGeometry(QtCore.QRect(75, 420, 130, 40))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.convertW.setFont(font)
        self.convertW.setObjectName("convertW")
        self.convertM = QtWidgets.QPushButton(self.centralwidget)
        self.convertM.setGeometry(QtCore.QRect(295, 420, 130, 40))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.convertM.setFont(font)
        self.convertM.setObjectName("convertM")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PyQonverter"))
        self.title.setText(_translate("MainWindow", "PyQonverter"))
        self.label.setText(_translate("MainWindow", "The simple media converter, powered by ffmpeg"))
        self.selFiles.setText(_translate("MainWindow", "Select Files"))
        self.queue.setText(_translate("MainWindow", "Current Queue:"))
        self.selFormat.setText(_translate("MainWindow", "Select Format"))
        self.convertW.setText(_translate("MainWindow", "Convert (Windows)"))
        self.convertM.setText(_translate("MainWindow", "Convert (macOS)"))

