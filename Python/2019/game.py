from os import system, name, path, makedirs
from time import sleep
from sys import stdout
import _thread
import random
import string

### Global Variables ###

# Player name
player = "Player"
# Menu
m = True
# Is game running
game = False
saveLocation = path.join(path.expanduser('~'),"Documents", "Yeetmeister")

# The choices made determine which SCENARIO occurs at which STAGE (scene) in the story.
scene = 1
scenario = 0

# Challenge 1 Timer
c1t = 2

# Challenge 1 Score limiter
c1c = 0

# Check if Challenge 1 Failed
c1f = False

# Amount of enter hits in the dodge scene, also the amount of times you get stabbed.
c = 0

# Custom feature 1: Save data. This dictionary gets saved to, and/or read from a file.
status = {
    "scene": "1",
    "scenario": "0",
    "inventory": []
}

# Custom feature 2: Slots for found items.
inventory = []


gf = None # me_irl
chad = "Chad"

fight = False

# Clear Screen
def clear():
    if name == 'nt': 
        _ = system('cls')
    else: 
        _ = system('clear')

# Main Menu
def menu():
    global player
    global m
    global scene
    global scenario
    e = 0
    l = 0
    while m:
        clear()
        print("Welcome", player, "\n")
        print("1. New Game")
        print("2. Load Game")
        print("3. Change name")
        print("4. Exit")
        # Print blank line when no error to keep spacing consistent
        if e == 0 and l == 0:
            print("\n")
        elif l == 0:
            print("\nInvalid Input")
        elif l == 1:
            print("\nFile loaded successfully!\n")
            l = 0
            sleep(1)
            startGame()
            m = False
            clear()
            break
        elif l == 2:
            print("\nNo save file found.")
            l = 0
        elif l == 3:
            print("\nError loading save file.")
            l = 0
        o = input("\nSelect option [1 - 4]: ")
        while o:
            # New Game
            if o == "1":
                startGame()
                m = False
                break
            # Load Save
            elif o == "2":
                if loadSave() == 1:
                    l = 1
                elif loadSave() == 2:
                    l = 2
                else:
                    l = 3
                break
            # Name change
            elif o == "3": 
                clear()
                player = input("Player Name: ")
                break
            # Exit
            elif o == "4":
                quit()
                break
            # Sneaky cut
            elif o.split()[0] == "gt":
                try:
                    scene = int(o.split()[1])
                    scenario = int(o.split()[2])
                except:
                    e += 1
                    continue
                sleep(1)
                startGame()
                m = False
                break
            else:
                e += 1
                break

# Load savedata from file
def loadSave():
    global scene
    global scenario
    global inventory
    success = 0
    if path.exists(path.join(saveLocation, "CYOA.txt")):
        with open(path.join(saveLocation, "CYOA.txt")) as f:
            global status 
            status = eval(f.read())
            try:
                scene = status["scene"]
                scenario = status["scenario"]
                inventory = status["inventory"]
                success = 1
            except:
                success = 3
    return success

# Saves the game
def saveGame():
    global status
    global scene
    global scenario
    global inventory
    status["scene"] = scene
    status["scenario"] = scenario
    status["inventory"] = inventory
    if not path.exists(path.join(saveLocation)):
        makedirs(path.join(saveLocation))
    with open(path.join(saveLocation, "CYOA.txt"), "w+") as f:
        f.seek(0)
        f.write(str(status))

# Custom feature maybe? Autosave
def autoSave():
    global autosave
    if autosave:
        saveGame()

# Start Game
def startGame():
    global game
    game = True

# Print letter by letter.
def sprint(t, d):
    for i in t:
        print(i, end = "")
        stdout.flush()
        if i == ".":
            sleep(1)
        elif i == ",":
            sleep(0.5)
        else:
            sleep(d)

# Mainly for ellipses
def sprintf(t, d):
    for i in t:
        print(i, end = "")
        stdout.flush()
        sleep(d)

# Pause length for "comma".
def comma():
    sleep(0.5)

# Pause length for "fullstop".
def fstop():
    sleep(1)

# Countdown timer for challenge 1
def c1Count():
    global c1t
    global c1f
    global c1c
    while c1t > 0 and c1c < 10 and not c1f:
        sleep(1)
        c1t -= 1
    if c1t == 0 and c1c != 10:
        c1f = True

def fightTimer():
    global fight
    fight = True
    sleep(5)
    fight = False

def getGF():
    global gf
    gfnames = ["Karen", "Lucy", "Stacy", "Christine", "Amy", "Hannah", "Snoozy", "Sophie"] # these names are totally random
    gf = gfnames[random.randint(0,7)]

def getChad():
    global chad
    chadnames = ["Chad", "Kyle", "Stoven", "Chris", "Rompert", "Richard", "Ryan", "John"] # these names are totally random
    chad = chadnames[random.randint(0,7)]

def getGFEye():
    eyeColours = ["blue", "green", "brown", "hazel", "grey", "amber", "red", "violet", "black"]
    return eyeColours[random.randint(0,8)]

# Dev mode, skips loading screen and some other stuff    
dev = 0

if dev != 1:
    # "Loading Screen"
    clear()
    sleep(1)
    sprint("Loading", 0.05)
    sleep(1)
    sprint("...", 1)
    sprint("\nGame Loaded!", 0.05)
    sleep(1)

    # Player Name
    clear()
    player = input("Player Name: ")

    # Autosave
    clear()
    autosave = input("Enable autosave [Y/n]: ")

# Auto enable autosave if user enters invalid option, else program honours yes or no.
try:
    autosave.toLower()
except:
    if dev == 1:
        autosave = False
    else:
        autosave = True
if autosave == "n" or autosave == "no" or dev == 1:
    autosave = False
else:
    autosave = True

menu()

# Game
while game:
    # Scene 1: Alarm
    while scene == 1:
        sleep(1)
        for i in range(3):
            clear()
            sprintf("...", 0.3)
            sleep(1)
        for i in range(3):
            clear()
            sprint("BRIIIIING!", 0.05)
            fstop()
        sprint("\nYour alarm goes off.", 0.05)
        sprint("\nWhat do you do?\n", 0.05)
        fstop()
        print("\n1. Hit snooze\n2. Get up\n3. Save and Exit")
        o = input("\n\n\nEnter an option [1-3]: ")
        while o:
            if o == "1":
                scenario = 1
                scene = 2
                break
            elif o == "2":
                scenario = 2
                scene = 2
                break
            elif o == "3":
                game = False
                saveGame()
                break
            else:
                clear()
                print("BRIIIIING!\nYour alarm goes off.\nWhat do you do?\n\n1. Hit snooze\n2. Get up\n3. Save and Exit\n\nInvalid Option\n")
                o = input("Enter an option [1-3]: ")

    # Scene 2: Consequences of previous actions
    while scene == 2:
        autoSave()
        # Skips to challenge if dev, skipping the story
        if dev != 1:
            # Snooze
            if scenario == 1:
                clear()
                sprint("You snooze your alarm.", 0.05)
                sprintf(" \"5 more minutes...\", ", 0.05)
                comma()
                sprint("you think to yourself.", 0.05)
                clear()
                for i in range(0, 3):
                    sprintf("zzzzzz...", 0.05)
                    fstop()
                clear()
                try:
                    sprint(player.upper() + "!!!", 0.03)
                except:
                    sprint(player + "!!!", 0.03)
                fstop()
                clear()
                sprint("You jolt out of bed and glance at the clock.\n", 0.03)
                sprint("8:03, ", 0.03)
                sprint("the bus comes in 2 minutes!", 0.03)
                fstop()
                clear()
                sprint("You jump out of bed and rush to get yourself ready.", 0.05)

            # Get up
            elif scenario == 2:
                clear()
                sprint("You get out of bed,", 0.05)
                sprint(" dazed and not fully awake.", 0.05)
                clear()
                sprint("You pack your bags, and get yourself ready.", 0.05)
                sprint(" You check the time: ", 0.05)
                comma()
                sprint("7:30.", 0.05)
                sprintf(" The bus doesn't come till 8:05...", 0.05)
                fstop()
                clear()
                sprint("You hop on the couch.", 0.05)
                sprint(" \"I'll just have a quick nap,\" you think to yourself.", 0.05)
                clear()
                for i in range(0, 3):
                    sprintf("zzzzzz...", 0.05)
                    fstop()
                try:
                    sprint(player.upper() + "!!!", 0.03)
                except:
                    sprint(player + "!!!", 0.03)
                fstop()
                clear()
                sprint("You jolt up, and glance at the clock.\n", 0.03)
                sprint("8:03, ", 0.03)
                sprint("the bus comes in 2 minutes!", 0.03)
                fstop()
                clear()
                sprint("You quickly grab your things, hastily checking that you've got everything.", 0.03)
                sprint(" Ah!", 0.03)
                fstop()
                sprint(" Almost forgot the chocolates you got for your girlfriend. How could you forget? It's valentines day.", 0.03)

            clear()
            sprint("You sprint out the door. Did you forget anything? ", 0.03)
            fstop()
            clear()
            sprint("Too late now, there's no turning back. ", 0.03)
            sprintf("You sprint along the rocky sidewalk, trying to make it to the bus station before the bus leaves...", 0.03)
            fstop()
            clear()

        # Challenge 1
        print("CHALLENGE: Sprint to the bus.")
        sleep(1.5)
        print("\nMake sure you don't lose your footing on the rocky footpath!")
        sleep(1.5)
        print("Type the opposite direction to the text on screen (i.e. if \"right\" is displayed, type in \"left\").")
        sleep(1.5)
        print("Inputs are not case sensitive.\n")
        sleep(1.5)
        print("Be quick, and make no mistakes!")
        sleep(3)

        # Challenge 1 Ready
        clear()
        print("CHALLENGE: Sprint to the bus.\n")
        sprintf("Ready...", 0.05)
        fstop()
        sprintf("Steady...", 0.05)
        fstop()
        print("GO!")
        fstop()
        # Custom feature 3: Threaded timer for the running challenge.
        _thread.start_new_thread(c1Count, ())

        # Challenge 1
        while c1c < 10 and c1t > 0 and not c1f:
            clear()
            dir = random.randint(1, 2)
            print("CHALLENGE: Sprint to the bus.\n")
            print("Ready...Steady...GO!",)
            if dir == 2:
                p = "left"
            else:
                p = "right"
            print("\n" + p + "\n")
            foot = input("Direction: ")
            if p == "left":
                if foot.lower() != "right":
                    c1f = True
                    break
                # For some reason code doesn't work if these 2 variables are left at bottom of while loop
                elif not c1f:
                    c1t = 2
                    c1c += 1
            else:
                if foot.lower() != "left":
                    c1f = True
                    break
                elif not c1f:
                    c1t = 2
                    c1c += 1
        if not c1f:
            # If snoozed
            if scenario == 1:
                scenario = 1
            else:
                scenario = 2
        elif c1f:
            if c1t <= 0:
                print("\nToo slow!")
                fstop()
            else:
                print("\nOh no!")
                fstop()
            # If snoozed
            if scenario == 1:
                scenario = 3
            else:
                scenario = 4
        scene = 3
        autoSave()

    while scene == 3:
        # If snooze and succeeded challenge:
        s = scenario
        if s == 1 or s == 2:
            if dev != 1:
                clear()
                sprint("PHEW!", 0.03)
                comma()
                sprint(" Made it to the bus on time.", 0.05)
                sprint("\nYou feel a little tired, and doze off on the bus.", 0.05)
                clear()
                for i in range(0, 3):
                    sprintf("zzz...", 0.05)
                    fstop()
                sprint("SKRRRRRT, PSSSSSST!!!", 0.05)
                fstop()
                sprint("\nYou arrive at school.", 0.05)
                sprint(" You rush to your lockers to put your things away.", 0.05)

                clear()
                sprint("You take out the chocolates you got for your girlfriend", 0.05)
                if s == 1:
                    sprint("...", 1)
                    sprint("\nOh.", 0.05)
                    sprintf(" The valentines day chocolates...", 0.05)
                    fstop()
                    sprintf("\nThat was what you forgot...", 0.05)
                    fstop()
                    sprintf("\nYou turn around...and there she is...", 0.05)
                    fstop()
                    clear()
                    sprint("\"Hey\", she says.", 0.05)
                    sprint("You shudder at the sound of her gorgeous voice.", 0.05)
                    sprintf("\n\"H...hey\", you reply, visibly sweating.", 0.05)
                    fstop()
                    clear()
                    sprint("\"I got you these chocolates, happy valentines day!\", she says with a smile, handing you a box of very expensive chocolates.\nYou take them, \"t-thanks, I got some for you as well but uhm...well...I forgot them in a rush this morning.\"", 0.05)
                    clear()
                    sprint("\"Oh...\" she replies, trying to hide her disappointment, \"well I've got class now, see you later, I guess,\" she says as she walks away.\nYou lift your hand as if you were going to say something, but then drop it again. You go off to class.", 0.05)
                    clear()

                elif s == 2:
                    sprint(", grateful that you checked before leaving the house this morning.", 0.05)
                    sprint(" You turn around and there she is, looking gorgeous as ever.", 0.05)
                    clear()
                    sprint("\"Hey,\" you greet her.\n\"Hey,\" she replies, slightly avoiding eye contact.", 0.05)
                    clear()
                    sprint("\"Happy valentines day!\", you smile as you hand her the box of chocolate.\nShe receives the box, \"thanks,\" she replies, still avoiding eye contact.\nShe walks off to class.\n\"I'll see you later love!\", you exclaim.\nShe doesn't respond...\nYou go off to class.", 0.05)
            
        elif s == 3 or s == 4:
            if dev != 1:
                clear()
                sprint("AAAAAAH!\n", 0.03)
                fstop()
                sprintf("You trip on the footpath, and faceplant...right into a muddy puddle.", 0.03)
                fstop()
                clear()
                sprint("You look up to see the bus leaving.", 0.05)
                sprintf(" Great, now you have to run to school to not be late, and in the 30˚C heat as well...", 0.05)
                fstop()
                clear()
                sprint("You make it to school, all sweaty and covered in mud. Everyone is looking at you. You try to ignore them as your put your stuff in your locker.", 0.05)
                clear()
                sprint("You turn around and see your girlfriend.\n\"Goodness, what happened to you?!\", she exclaims.\n\"Oh I just tripped whilst running for the bus, no big deal,\" you reply, trying to sound casual. Your girlfriend is not impressed.", 0.05)
                clear()
                sprint("\"Happy valentines day love,\" you say as you rummage through your bag looking for the chocolates you got her.", 0.05)
            if s == 3:
                clear()
                sprint("...oh. That was what you forgot.", 0.05)
                clear()
                sprint("You drop your bag, and stand there awkwardly for a second.\n\"I um, forgot the chocolates,\" you say with an awkward smile.", 0.05)
            elif s == 4:
                clear()
                sprint("\"Ah! There it is!\", you hand her the box, only problem: you crushed it when you tripped and fell. She looks at the crushed box.", 0.05)
            
            sprint("\nYour girlfriend rolls her eyes and walks away.\n\"Wait!\", you try to call her back, but she ignores you. Dejected, you sort out the rest of your bag, and head off to class.", 0.05)

        # Feature 2: Found item
        clear()
        sprint("You find a knife on the ground on the way to class.", 0.05)
        sprint("\nWhat do you do?\n", 0.05)
        fstop()
        print("\n1. Pick it up\n2. Leave it\n3. Save and Exit")
        o = input("\n\n\nEnter an option [1-3]: ")
        while o:
            if o == "1":
                inventory.append("Knife")
                sprint("\nYou pick up the knife.", 0.05)
                break
            elif o == "2":
                sprint("\nYou ignore the knife.", 0.05)
                break
            #elif o == "menu":
            elif o == "3":
                game = False
                saveGame()
                break
            else:
                clear()
                print("You find a knife on the ground.\nWhat do you do?\n\n1. Pick it up\n2. Leave it\n3. Save and Exit\n\nInvalid Option\n")
            o = input("Enter an option [1-3]: ")
        scene = 4
        autoSave()
    while scene == 4:
        clear()
        if dev != 1:
            sprint("You go through the rest of the normal day of school without seeing your girlfriend. The interaction in the morning dwells in your mind, despite your attempts to avoid it. The day ends, and you go out to the carpark to meet your girlfriend. ", 0.05)

            getGF() # if only it was this easy lmao
            getChad()

            if scenario <= 2:
                sprint(f"\"Hey {gf},\" you greet her. You go in for a kiss, but she dodges you. Oof.\n\"We need to talk,\" she says. You listen in.", 0.05)
                clear()
                sprint(f"Look {player.capitalize()}, I love you, I really do, but I just don't think we can work out, you know, personality wise...I think we should break up.", 0.05)
                clear()
                sprint(f"Break up, ouch. The words hit you like a swarm of wasps. You struggle to process this.\n\"You've been going out with {chad} haven't you? He told you to do this didn't he?!\" you exclaim with anger and frustration.\n\"No Please {player.capitalize()}, I swear I-\"\nYou stopped listening to her.", 0.05)
                clear()
                if chad == "Kyle":
                    sprint(f"Filled with blinding rage, you go out looking to fight {chad}. You find him alone, drinking his 40 cans of Monster Energy™ near an alleyway. You push him into the alleyway.", 0.05) # all Kyles are like this no?
                else:
                    sprint(f"Filled with blinding rage, you go out looking to fight {chad}. You find him alone, near an alleyway. You push him into the alleyway.", 0.05)
                clear()
                sprint("\"Oi what was that for?\", he asks.\n\"I'm going to beat the sh!t out of you for stealing my girl!\" you yell.\nHe scoffs and readies his fists,\"well come at me then!\"\nYou try to look tough, but you weren't expecting to actually get into a fight. You are not prepared at all.", 0.05)
            
            else:
                sprint("You find her in the carpark, only she is making out with someone else...", 0.05)
                clear()
                sprint(f"\"{gf}?!\" She glances over at you.\n\"Oh how nice of you to join us {player.capitalize()}\", she replies.\n\"How could you do this to me?!\" you exclaim on the verge of tears.", 0.05)
                clear()
                sprint(f"\"Oh it's simple really. I just can't deal with your tardiness anymore. You are always getting into trouble and dragging me into it. Do you know how embarrassing it was when you showed up this morning? {chad} here would never do that to me isn't that right love?\" she kisses him.\nEvery word she said hurt like an unauthorised castration. You're filled with anger.", 0.05)
                clear()
                sprint(f"You push {chad} away, and strike him in the face.\"NO STOP!\" {gf} screams, but you and {chad} are already tangled in a fight.", 0.05)
        
        # Challenge 2: Fight
        clear()
        sprint("He throws a punch at you.", 0.05)
        print("\n\n(Spam enter to dodge)\n")
        fstop()
        sprintf("Ready...Steady...", 0.05)
        fstop()
        print("GO! Hits: 0")
        fight = True # code doesn't work when this line is here and idk why so i'm leaving it
        _thread.start_new_thread(fightTimer, ())
        while fight:
            clear()
            print("He throws a punch at you.\n\n(Spam enter to dodge)\n\nReady...Steady...GO! Hits: " + str(c) + "\n")
            o = input("")
            c += 1
        clear()
        print("He throws a punch at you.\n\n(Spam enter to dodge)\n\nReady...Steady...GO! Hits: " + str(c) + "\n")
        print("\nHit enter", c, "times!")
        sleep(2)

        if c < 40:
            scenario += 12
            scene = 5
            break
        
        clear()

        sprint("You successfully dodge his first hit, but he gets you with his other fist. You hit the ground.", 0.05)

        if "Knife" in inventory:
            clear()
            sprint("You feel your pocket, and notice the knife that you had picked up earlier.\n", 0.05)
            sprint("What do you do?\n", 0.05)
            fstop()
            print("\n1. Use it\n2. Don't use it\n\n\n")
            o = input("Enter an Option [1-2]: ")
            # Not using while o because that registers the enter key spams if there were any leftover.
            while scene == 4:
                if o == "1":
                    scene = 5
                    autoSave()
                    break
                elif o == "2":
                    scenario += 4
                    scene = 5
                    break
                elif o != "":
                    clear()
                    print("You feel your pocket, and notice the knife that you had picked up earlier.\nWhat do you do?\n\n1. Use it\n2. Don't use it\n\nInvalid option\n")
                    o = input("Enter an Option [1-2]: ")
                else:
                    clear()
                    print("You feel your pocket, and notice the knife that you had picked up earlier.\nWhat do you do?\n\n1. Use it\n2. Don't use it\n\n\n")
                    o = input("Enter an Option [1-2]: ")

        else:
            scenario += 8
            scene = 5
        
        # We don't autosave if no knife in inventory so that we can replay the previous scene.

    while scene == 5:
        # You stab chad
        if scenario <= 4:
            print("todo")

        # You don't stab chad
        elif scenario > 4 and scenario <= 8:
            clear()
            sprint(f"Being cocky, and also afraid of the consquences of using a knife (mostly the latter option), you decide to take on {chad} in a fistfight. But, {chad} being stronger, faster, and fitter than you, did not increase your chances, and he hits you square in the face with a punch.", 0.05)
            clear()
            sprint("You fall, and hit your head on the corner of a dumpster. The sharp corner impales your skull, and you die instantly. Your body is thrown into a dumpster in the alley.", 0.05)

        # You didn't pick up the knife
        elif scenario > 8 and scenario <= 12:
            clear()
            sprint(f"You struggle to get yourself back up, but before you can get yourself back into a fighting stance, {chad} grabs you by the shirt, and holds you up against a wall.", 0.05)
            clear()
            sprint(f"He pulls out the knife that you didn't pick up, and stabs you {str(c)} times. You die, wreathing in pain. Your body is thrown into a dumpster in the alley.", 0.05)

        # You didn't dodge the first hit
        else:
            clear()
            sprint(f"You weren't fast enough, and got instantly decked by {chad}.", 0.05)
            clear()
            sprint("Your skull caves in, and you die instantly from the blunt force trauma.", 0.05)
        
        scene = 6

    while scene == 6 and game:
        if scenario <= 4:
            if dev != 1:
                clear()
                sprint(f"You get up, and manage to somehow knock {chad} to the ground. You pull out the knife, and stab {chad} {str(c)} times, ignoring {gf}'s cries for you to stop. {chad} dies on the spot, and you throw his body into a dumpster.", 0.05)
                clear()
                sprint(f"You snap out of your hate filled frenzy, and look down at your bloody hands. You've just killed a man. You look at {gf}, staring into her tear filled eyes. Those beautiful {str(getGFEye())} eyes.", 0.05)
                clear()
                sprint(f"You run away from the scene, unable to deal with the consequences of what you have done. You run to the bridge where you and {gf} first kissed. Oh that moment seems so far gone. If only you could go back to those times.", 0.05)
                if scenario == 1 or scenario == 4:
                    clear()
                    sprint(f"You hear {gf}'s voice, calling out to you.", 0.05)
                clear()
            sprint("You climb over the railing, having lost your will to live, and unable to bear the consequences of your actions. The warm summer breeze strokes your cheeks. You close your eyes.", 0.05)
            print("\n\n1. Jump\n2. Go back\n3. Save and Exit\n\n\n")
            o = input("Enter an option [1-3]: ")
            while o:
                clear()
                if o == "1" or o == "2":
                    if o == "1":
                        sprint("You jump. You pass out on the way down. Everything fades to black. You die on impact with the water. ", 0.05)

                    elif o == "2":
                        sprint("You don't have the guts to jump, and do not want leave the burden of death on your loved ones. You chicken out, and try to climb back over the railing to safety.", 0.05)
                        clear()
                        sprint("You slip. Everything slows down as you fall to your death.", 0.05)
                        if scenario == 1 or scenario == 4:
                            sprint(f"You see {gf} leaning over the railing, crying and reaching out to you. You close your eyes.", 0.05)
                            clear()
                        sprint("You die on impact with the water below. ", 0.05)
                    
                    clear()

                    # Ending 1
                    if scenario == 1:
                        clear()
                        sprint(f"{chad} was in fact not involved in {gf}'s decision to break up wih you. Her decision was in the moment, caused by her disappointment in your lack of responsibility.", 0.05)
                        clear()
                        sprint(f"Feeling as if she was the cause of your death, and the murder of the innocent {chad}, she climbs over the railing and jumps to her death.", 0.05)
                        clear()
                        sprint(f"{gf}'s cries forever haunt you in the afterlife, reminding you of the burden and grief you have caused on your family, {chad}'s family, and her family. The image of {gf} crying as she watched you fall to your death is forever burned into your mind. Your soul is forever tortured, and filled with unending guilt.", 0.05)
                    # Ending 2
                    elif scenario == 2:
                        clear()
                        sprint(f"{gf}'s decision to break up with you was in fact not related to {chad}, but rather it was your rash decision making that prompted her to do so. You relentless murder spree was a prime example of that.", 0.05)
                        clear()
                        sprint(f"{gf}, whilst grateful to have avoided the ticking time bomb that was you, is filled with the guilt, knowing she caused your death, and the death of the innocent {chad}. She falls into a depression, and contemplates suicide, however she is loved by many, and they help her guide her away from the void.", 0.05)
                        clear()
                        sprint(f"Despite your numerous shortcomings, {gf} could not forget the beautiful memories that you had given her. Whilst memories of the bridge are cursed in her memory, all the other happy memories of you and her remain. You rest easy, knowing that you are still loved and remembered for the good in you.", 0.05)
                    # Ending 3
                    elif scenario == 3:
                        clear()
                        sprint(f"Having long lost her feelings about you, {gf} mourns the death of her new beloved, {chad}. She blames you for his death, but inside she still feels guilty for driving you to murder. Her inner conflict consumes her, and she spirals into addiction. She slowly becomes more and more depressed, until she kills herself with a razor in the bathtub in her apartment, 3 months after you death.", 0.05)
                        clear()
                        sprint("Her spirit still haunts you in the afterlife, screaming at you, still blaming you for everything that has happened. You are forever tormented for your actions, and the grief that they have caused.", 0.05)
                    # Ending 4
                    else:
                        clear()
                        sprint(f"Despite having lost her feelings for you, deep down, {gf} still cared about you. Your suicide, compounded with the death of her new beloved, {chad} overwhelm her, and she hangs herself in her bedroom that night.", 0.05)
                        clear()
                        sprint("Her cries echo throughout the afterlife, and consume you, cursing you to an eternity of guilt. You are forever sentenced to guilt and reliving the events of the past.", 0.05)
                    break

                elif o == "3":
                    saveGame()
                    quit()
                
                else:
                    print("You climb over the railing, having lost your will to live, and unable to bear the consequences of your actions. The warm summer breeze strokes your cheeks. You close your eyes.\n\n1. Jump\n2. Go back\n3. Save and Exit\n\nInvalid option\n")
                    o = input("Enter an option [1-3]: ")

        # Ending 5
        elif scenario == 5 or scenario == 9 or scenario == 13:
            clear()
            sprint(f"It was indeed not {chad} who made {gf} break up with you, and she would come to realise that she did not in fact want to break up with you. She was acting under the heat of the moment, and her disappointment in your lack of responsibility caused her to act irrationally.", 0.05)
            clear()
            sprint(f"Unable to deal with your death, and feeling as if she was the cause, {gf} spirals into a deep depression. Compounded by tensions in the family, her mental health rapidly declines.", 0.05)
            clear()
            sprint(f"Three days later, {gf} commits suicide by jumping off the bridge where she first kissed you.\nHer spirit forever haunts you in the afterlife, torturing you for eternity.", 0.05)

        # Ending 6   
        elif scenario == 6 or scenario == 10 or scenario == 14:
            clear()
            sprint(f"{chad} had in fact not been involved in the breakup between you and {gf}. It was your rash decision making and hot headedness that prompted her. You rushing into conflict was a prime example of this.", 0.05)
            clear()
            sprint(f"Although your death was devastating for {gf}, in the end she was able to move on, and was slightly glad to have dodged the ticking time bomb that you were. She would go on to be loved, and go on living a prosperous and happy life.", 0.05)
            clear()
            sprint(f"Whilst she was glad to have avoided your flaws, she could not forget the beautiful memories that you had given her, and your first kiss on the bridge across the river, will remain in her memory, serving as a reminder of your good qualities. You rest easy, knowing that you are still loved and remembered for the good in you, and not all the bad.", 0.05)

        # Ending 7
        elif scenario == 7 or scenario == 11 or scenario == 15:
            clear()
            sprint(f"Having long stopped caring about the irresponsible headache that was you, {gf} did not care the slightest about your death. She and {chad} would go on to be a power couple, and have many children over the years. If it makes you feel better, their ugliest child is named {player} after you.", 0.05)
            clear()
            sprint("Aside from that, you are forgotten. Your name is never mentioned, and nothing that you've done remains. Your parents never loved you, and in fact are quite glad that you are dead. You are depressed in the afterlife, and are doomed to an eternity of crying.", 0.05)

        # Ending 8
        else:
            clear()
            sprint(f"Whilst {gf} had lost her feelings towards you, she could not forget all the good things you've done for her. She is conflicted, and her relationship with {chad} crumbles, as she cannot forgive him for your death. She spirals into a deep depression, turning to drugs and prostitution. In the end, her guilt consumes her. She dies from a drug overdose, 3 years after your death.", 0.05)
            clear()
            sprint("You constantly hear her tortured soul scream in the afterlife. You feel forever guilty for your mistakes, and blame yourself for everything that happened.", 0.05)

        sprint("\n\nThe End.", 0.25)
        fstop()
        clear()
        sprint("Play again?", 0.05)
        fstop()
        print("\n\n1. New Game\n2. Load last save\n3. Exit\n\n\n")
        o = input("Enter an option [1-3]: ")
        x = True
        while x:
            if o == "1":
                clear()
                player = input("Player Name: ")
                clear()
                autosave = input("Enable autosave [Y/n]: ")
                try:
                    autosave.toLower()
                except:
                    if dev == 1:
                        autosave = False
                    else:
                        autosave = True
                if autosave == "n" or autosave == "no" or dev == 1:
                    autosave = False
                else:
                    autosave = True
                scene = 1
                scenario = 0
                inventory.clear()
                startGame()
                x = False
                break
            elif o == "2":
                if loadSave() == 1:
                    clear()
                    print("\n\n1. New Game\n2. Load last save\n3. Exit\n\nFile loaded successfully!\n\n")
                    fstop()
                    clear()
                    x = False
                    break
                elif loadSave() == 2:
                    clear()
                    print("\n\n1. New Game\n2. Load last save\n3. Exit\n\nNo save file found.\n")
                    o = input("Enter an option [1-3]: ")
                    continue
                else:
                    clear()
                    print("\n\n1. New Game\n2. Load last save\n3. Exit\n\nError loading save file.\n")
                    o = input("Enter an option [1-3]: ")
                    continue
            elif o == "3":
                quit()
                break
            elif o.split()[0] == "gt":
                clear()
                try:
                    scene = int(o.split()[1])
                    scenario = int(o.split()[2])
                    sleep(1)
                    startGame()
                    x = False
                    break
                except:
                    print("\n\n1. New Game\n2. Load last save\n3. Exit\n\nInvalid input\n")
                    o = input("Enter an option [1-3]: ")
            else:
                clear()
                print("\n\n1. New Game\n2. Load last save\n3. Exit\n\nInvalid input\n")
                o = input("Enter an option [1-3]: ")