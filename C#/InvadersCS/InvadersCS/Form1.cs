﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvadersCS
{
    public partial class Form1 : Form
    {
        // We use a list to store the aliens. Lists are useful because we can add and remove items easily.
        List<PictureBox> arrAliens = new List<PictureBox>();
        List<PictureBox> arrSpaz = new List<PictureBox>(); // List for crazybois
        List<PictureBox> arrSingle = new List<PictureBox>();
        List<PictureBox> arrMisc = new List<PictureBox>();
        List<PictureBox> arrAss = new List<PictureBox>();
        PictureBox ship = new PictureBox();

        // Alien Movement Direction. True = Right, False = Left
        bool moveDir = true;

        // Booleans for player movement
        bool moveLeft = false;
        bool moveRight = false;
        bool moveUp = false;
        bool moveDown = false;
        bool shooting = false;

        // Check if player is dead, and whether the player has run out of lives
        bool dead = false;
        bool gameOver = false;
        bool game = false;

        // Checks required for spawning special entities
        bool contact = false;
        bool single = false;
        bool spazActive = false;
        int spazDir = 0;
        bool assDir = true;
        bool boss = false;

        // FLEX
        int lives = 3;
        int score = 0;
        
        // Random choice generator
        Random r = new Random();
        int choice = 0;

        SoundPlayer music = new SoundPlayer(Properties.Resources.Gustavo_Dudamel__Dvorak___Symphony_no__9___4th_movement___Allegro_con_fuoco);
        bool playing = true;

        SoundPlayer go = new SoundPlayer(Properties.Resources.Game_Over___Super_Mario_64);

        // ---------------
        // Initial Loading
        // ---------------

        public Form1()
        {
            InitializeComponent();
            this.KeyDown += Form1_KeyDown;
            this.KeyUp += Form1_KeyUp;
        }

        // Stuff to load when game is reset
        private void Form1_Load(object sender, EventArgs e)
        {
            startGame();
            music.PlayLooping();
        }

        // Loads game settings
        private void startGame()
        {
            label1.Visible = true;
            label2.Visible = true;
            createAliens();
            loadScores();
            createShip();
            ship = (PictureBox)Controls["ship"];
            game = true;
        }

        // The most important part of the game ;)
        private void loadScores()
        {
            label1.Text = "Score: " + score.ToString();
            label2.Text = "Lives: " + lives.ToString();
        }

        private void endGame()
        {
            gameOver = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            SystemSounds.Hand.Play();
            music.Stop();
            go.Play();
        }

        // ---------------
        //  Player Input
        // ---------------

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                shooting = true;
            }
            if (e.Modifiers == Keys.Space)
            {
                shooting = true;
            }
            if (e.KeyCode == Keys.A)
            {
                moveLeft = true;
            }
            else if (e.KeyCode == Keys.D)
            {
                moveRight = true;
            }
            else if (e.KeyCode == Keys.W)
            {
                moveUp = true;
            }
            else if (e.KeyCode == Keys.S)
            {
                moveDown = true;
            }
            // Debug
            else if (e.KeyCode == Keys.Q)
            {
                SystemSounds.Asterisk.Play();
                Application.Exit();
            }
            else if (e.KeyCode == Keys.R)
            {
                Application.Restart();
            }
            else if (e.KeyCode == Keys.Z)
            {
                endGame();
            }
            else if (e.KeyCode == Keys.M)
            {
                if (playing)
                {
                    music.Stop();
                    playing = false;
                }
                else if (!playing)
                {
                    music.PlayLooping();
                    playing = true;
                }
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                shooting = false;
            }
            if (e.Modifiers == Keys.Space)
            {
                shooting = true;
            }
            if (e.KeyCode == Keys.A)
            {
                moveLeft = false;
            }
            else if (e.KeyCode == Keys.D)
            {
                moveRight = false;
            }
            else if (e.KeyCode == Keys.W)
            {
                moveUp = false;
            }
            else if (e.KeyCode == Keys.S)
            {
                moveDown = false;
            }
        }

        // ---------------
        // Entity Creation
        // ---------------

        private void createShip()
        {
            PictureBox ship = new PictureBox();
            ship.Size = new Size(20, 20);
            ship.BackColor = Color.Red;
            ship.Name = "ship";
            ship.Location = new Point(this.Width / 2, this.Height - 100);
            Controls.Add(ship);
        }

        private void createAliens()
        {
            if (arrAliens.Count == 0 && !boss) 
            {
                for (int x = 0; x <= 10; x++)
                {
                    for (int y = 0; y <= 3; y++)
                    {
                        PictureBox p = new PictureBox();
                        p.Size = new Size(20, 20);
                        p.Location = new Point(60 + (30 * x), 70 + (30 * y));
                        p.BackColor = Color.LimeGreen;
                        p.Name = "alien";
                        Controls.Add(p);
                        arrAliens.Add(p);
                    }
                }
            }
        }

        // Feature 1: Alien array that attacks diagonally
        private void createSpaz()
        {
            // Check Spaz related stuff
            if (!spazActive && score != 0 && arrSpaz.Count == 0)
            {
                for (int x = 0; x <= 2; x++)
                {
                    for (int y = 0; y <= 2; y++)
                    {
                        PictureBox p = new PictureBox();
                        p.Size = new Size(20, 20);
                        switch (choice)
                        {
                            case 1:
                                p.Location = new Point(-100 + (30 * x), -100 + (30 * y));
                                spazDir = 1;
                                break;
                            case 2:
                                p.Location = new Point(700 + (30 * x), -100 + (30 * y));
                                spazDir = 2;
                                break;
                            case 3:
                                p.Location = new Point(-100 + (30 * x), 450 + (30 * y));
                                spazDir = 3;
                                break;
                            case 4:
                                p.Location = new Point(700 + (30 * x), 450 + (30 * y));
                                spazDir = 4;
                                break;
                        }
                        p.BackColor = Color.Blue;
                        p.Name = "spaz";
                        Controls.Add(p);
                        arrSpaz.Add(p);
                    }
                spazActive = true;
                }
            }
        }

        // Feature 2: 1-Up
        private void createLife()
        {
            PictureBox health = new PictureBox();
            Random l = new Random();
            int x = l.Next(50, this.Width - 50);
            health.Size = new Size(15, 15);
            health.BackColor = Color.Yellow;
            health.Name = "life";
            health.Location = new Point(x, -20);
            Controls.Add(health);
            arrMisc.Add(health);
        }

        // Feature 3: Purple thingo that falls from the sky
        private async void createSingle()
        {
            if (!single)
            {
                PictureBox p = new PictureBox();
                Random l = new Random();
                int x = l.Next(50, this.Width - 50);
                p.Size = new Size(20, 20);
                p.BackColor = Color.Purple;
                p.Name = "single";
                p.Location = new Point(x, -20);
                Controls.Add(p);
                arrSingle.Add(p);
                single = true;
                await Task.Delay(2000);
                single = false;
            }
        }

        // Feature 4: Asteroids
        private void createAss()
        {
            if (arrAss.Count < 5 && choice > 2)
            {
                PictureBox p = new PictureBox();
                Random l = new Random();
                int x = l.Next(125, this.Width - 125);
                p.Size = new Size(50, 50);
                p.BackColor = Color.WhiteSmoke;
                p.Name = "asteroid";
                p.Tag = "alive";
                p.Location = new Point(x, -20);
                Controls.Add(p);
                arrAss.Add(p);
            }
        }

        // ---------------
        // Entity Movement
        // ---------------

        private void moveShipVert()
        {
            if (moveUp && ship.Top >= -ship.Height + 100)
            {
                ship.Top -= 20;
            }
            else if (moveDown && ship.Top <= this.Height - 100)
            {
                ship.Top += 20;
            }
        }

        private void moveShipHor()
        {
            if (moveLeft)
            {
                ship.Left -= 20;
            }
            else if (moveRight)
            {
                ship.Left += 20;
            }

            if (ship.Left > this.Width)
            {
                ship.Left = -ship.Width;
            }
            else if (ship.Left < -ship.Width)
            {
                ship.Left = this.Width;
            }
        }

        private void moveShell()
        {
            foreach (PictureBox p in Controls.OfType<PictureBox>())
            {
                if (p.Name == "shell")
                {
                    if (p.Top <= 0)
                    {
                        p.Dispose();
                    }
                    else
                    {
                        p.Top -= 20;
                    }
                }

                if (p.Name == "bullet")
                {
                    if (p.Top > this.Height)
                    {
                        p.Dispose();
                    }
                    else
                    {
                        p.Top += 20;
                    }
                }
            }
        }

        private void alienMovement()
        {
            if (arrAliens.Count != 0)
            {
                foreach (PictureBox alien in arrAliens)
                {
                    if (moveDir == true)
                    {
                        alien.Left += 10;
                    }
                    else
                    {
                        alien.Left -= 10;
                    }
                }
            }
        }

        private void checkAlien()
        {
            if (arrAliens.Count != 0)
            {
                PictureBox first = arrAliens.First();
                PictureBox last = arrAliens.Last();

                foreach (PictureBox alien in arrAliens)
                {
                    if (alien == last)
                    {
                        if (alien.Left >= this.Width - 50)
                        {
                            moveDir = false;
                            if (last.Top <= this.Height - 150)
                            {
                                impendingDoom();
                            }
                        }
                        else if (first.Left <= 25)
                        {
                            moveDir = true;
                            if (last.Top <= this.Height - 150)
                            {
                                impendingDoom();
                            }
                        }
                    }
                }
            }
        }

        private void impendingDoom()
        {
            foreach (PictureBox alien in arrAliens)
            {
                alien.Top += 10;
            }
        }

        private void spazMovement()
        {
            if (arrSpaz.Count != 0)
            {
                foreach (PictureBox spaz in arrSpaz)
                {
                    switch (spazDir)
                    {
                        // Spawns top left and moves down right
                        case 1:
                            spaz.Left += 20;
                            spaz.Top += 15;
                            break;
                        // Spawns top right and moves down left
                        case 2:
                            spaz.Left -= 20;
                            spaz.Top += 15;
                            break;
                        // Spawns bottom left and moves up right;
                        case 3:
                            spaz.Left += 20;
                            spaz.Top -= 15;
                            break;
                        // Spawns bottom right and moves up left;
                        case 4:
                            spaz.Left -= 20;
                            spaz.Top -= 15;
                            break;
                    }
                }
            }
        }

        private void lifeMovement()
        {
            if (arrMisc.Count != 0)
            {
                foreach (PictureBox life in arrMisc)
                {
                    life.Top += 15;
                }
            }
        }

        private void singleMovement()
        {
            if (arrSingle.Count != 0)
            {
                foreach (PictureBox p in arrSingle)
                {
                    p.Top += 25;
                }
            }
        }

        private void moveAss()
        {
            if (arrAss.Count != 0)
            {
                foreach (PictureBox p in arrAss)
                {
                    p.Top += 10;
                    switch (choice)
                    {
                        case 1:
                            if (p.Left == 0)
                            {
                                assDir = true;
                            }
                            else if (p.Left == 550)
                            {
                                assDir = false;
                            }

                            if (assDir)
                            {
                                p.Left += 10;
                            }
                            else if (!assDir)
                            {
                                p.Left -= 10;
                            }
                            break;
                        case 2:
                            if (assDir)
                            {
                                assDir = false;
                            }
                            else if (!assDir)
                            {
                                assDir = true;
                            }
                            break;
                    }
                }
            }
        }

        // ---------------
        //    Shooting
        // ---------------

        // Aliens rain hell if player is not dead and there are aliens left
        private void shootAtShip()
        {
            if (arrAliens.Count > 0)
            {
                int x = r.Next(0, arrAliens.Count);
                PictureBox p = new PictureBox();
                p.Size = new Size(3, 5);
                p.BackColor = Color.Yellow;
                p.Location = new Point(arrAliens[x].Left + (arrAliens[x].Width / 2), arrAliens[x].Bottom);
                p.Name = "bullet";
                Controls.Add(p);
            }
        }

        // Player Shooting
        // Whenever we shoot we create a new bullet object. We will use name 'shell' to pick them up later
        // Go through all the picture boxes and move the important ones
        private void shootShell()
        {
            if (shooting && !dead)
            {
                PictureBox p = new PictureBox();
                p.Size = new Size(3, 5);
                p.Location = new Point(ship.Left + (ship.Width / 2), ship.Top);
                p.Name = "shell";
                p.BackColor = Color.Magenta;
                Controls.Add(p);
            }
        }

        // ---------------
        //  Hit Detection
        // ---------------

        private async void checkHits()
        {
            // When alien comes in contact with anything
            foreach (PictureBox a in arrAliens)
            {
                foreach (PictureBox p in Controls.OfType<PictureBox>())
                {
                    if (p.Name == "shell" && p.Bounds.IntersectsWith(a.Bounds))
                    {
                        p.BackColor = this.BackColor;
                        a.BackColor = this.BackColor;
                        p.Tag = "dead";
                        a.Tag = "dead";
                        score += 100;
                        speedSpawn();
                    }
                }

                if (a.Bounds.IntersectsWith(ship.Bounds) && !dead)
                {
                    resetShip();
                }
            }

            // When asteroid comes in contact with anything
            foreach (PictureBox a in arrAss)
            {
                foreach (PictureBox p in Controls.OfType<PictureBox>())
                {
                    if (p.Name == "shell" && p.Bounds.IntersectsWith(a.Bounds))
                    {
                        if ((string)a.Tag == "alive")
                        {
                            p.BackColor = this.BackColor;
                            a.Tag = "dying";
                            a.BackColor = Color.DimGray;
                            p.Tag = "dead";
                        }
                        else if ((string)a.Tag == "dying")
                        {
                            p.BackColor = this.BackColor;
                            a.Tag = "dead";
                            a.BackColor = this.BackColor;
                            p.Tag = "dead";
                            score += 300;
                        }
                    }

                    if (a.Bounds.IntersectsWith(ship.Bounds) && !dead)
                    {
                        resetShip();
                    }
                }
            }

            // Other entities
            foreach (PictureBox s in Controls.OfType<PictureBox>())
            {
                foreach (PictureBox p in Controls.OfType<PictureBox>())
                {
                    if (s.Name == "spaz")
                    {
                        if (p.Name == "shell" && p.Bounds.IntersectsWith(s.Bounds))
                        {
                            p.BackColor = this.BackColor;
                            s.BackColor = this.BackColor;
                            p.Tag = "dead";
                            s.Tag = "dead";
                            score += 200;
                            speedSpawn();
                        }

                        if (s.Bounds.IntersectsWith(ship.Bounds) && !dead)
                        {
                            resetShip();
                        }
                    }

                    if (s.Name == "single")
                    {
                        if (p.Name == "shell" && p.Bounds.IntersectsWith(s.Bounds))
                        {
                            p.BackColor = this.BackColor;
                            s.BackColor = this.BackColor;
                            p.Tag = "dead";
                            s.Tag = "dead";
                            score += 150;
                            speedSpawn();
                        }

                        if (s.Bounds.IntersectsWith(ship.Bounds) && !dead)
                        {
                            resetShip();
                        }
                    }

                    if (s.Name == "life")
                    {
                        if (p.Name == "shell")
                        {
                            if (s.Bounds.IntersectsWith(p.Bounds))
                            {
                                p.BackColor = this.BackColor;
                                s.BackColor = this.BackColor;
                                p.Tag = "dead";
                                s.Tag = "dead";
                                lives += 1;
                            }
                        }

                        if (s.Bounds.IntersectsWith(ship.Bounds) && !contact && !dead)
                        {
                            s.Dispose();
                            arrMisc.Remove(s);
                            lives += 1;
                            contact = true;
                            await Task.Delay(500);
                            contact = false;
                        }

                        if (choice == 2)
                        {
                            createSpaz();
                        }
                    }

                    if (s.Name == "bullet" && p.Name == "shell")
                    {
                        if (s.Bounds.IntersectsWith(p.Bounds))
                        {
                            p.BackColor = this.BackColor;
                            s.BackColor = this.BackColor;
                            p.Tag = "dead";
                            s.Tag = "dead";
                            score += 50;
                            speedSpawn();
                        }
                    }
                }
            }

            // When alien gel hits anything
            foreach (PictureBox p in Controls.OfType<PictureBox>())
            {
                if (p.Name == "bullet" && p.Bounds.IntersectsWith(ship.Bounds) && !dead)
                {
                    p.Tag = "dead";
                    resetShip();
                }
            }

            // Clean up anything that died
            foreach (PictureBox p in Controls.OfType<PictureBox>())
            {
                if ((string)p.Tag == "dead")
                {
                    p.Dispose();

                    if (p.Name == "alien")
                    {
                        arrAliens.Remove(p);
                    }
                    
                    if (p.Name == "spaz")
                    {
                        arrSpaz.Remove(p);
                        spazActive = false;
                    }
                }
            }

            // Update score display
            loadScores();
        }

        // Check for things that go off the screen
        private void checkFloat()
        {
            foreach (PictureBox p in Controls.OfType<PictureBox>())
            {
                if (p.Name == "spaz")
                {
                    if (p.Top > this.Height + 300)
                    {
                        p.Dispose();
                        arrSpaz.Remove(p);
                        spazActive = false;
                    }
                    else if (p.Top < -300)
                    {
                        p.Dispose();
                        arrSpaz.Remove(p);
                        spazActive = false;
                    }
                }

                if (p.Name == "life")
                {
                    if (p.Top > this.Height + 20)
                    {
                        p.Dispose();
                        arrMisc.Remove(p);
                    }
                }

                if (p.Name == "single")
                {
                    if (p.Top > this.Height + 20)
                    {
                        p.Dispose();
                        arrSingle.Remove(p);
                    }
                }

                if (p.Name == "asteroid")
                {
                    if (p.Top > this.Height + 70)
                    {
                        p.Dispose();
                        arrAss.Remove(p);
                    }
                }
            }
        }

        // Resets Ship
        private async void resetShip()
        {
            dead = true;
            lives--;
            ship.Visible = false;

            if (lives == 0)
            {
                endGame();
            }

            if (!gameOver)
            {
                await Task.Delay(3000);
                ship.Location = new Point(this.Width / 2, this.Height - 100);
                ship.Visible = true;
                await Task.Delay(500);
                dead = false;
            }
        }

        // ---------------
        //     Timers
        // ---------------

        private void speedSpawn()
        {
            if (score % 5000 == 0 && score != 0)
            {
                if (timer3.Interval > 500)
                {
                    timer3.Interval -= 20;
                }

                if (timer5.Interval > 500)
                {
                    timer5.Interval -= 20;
                }
            }

            if (score % 15000 == 0 && score != 0)
            {
                if (timer6.Interval > 10)
                {
                    timer6.Interval -= 5;
                }
            }
        }
        
        // General Timer
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                moveShipVert();
                moveShipHor();
                shootShell();
                moveShell();
                createAliens();
                choice = r.Next(1, 5);
                checkFloat();
                lifeMovement();
                createSingle();
            }
        }

        // Enemy shoot timer
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                shootAtShip();
            }
        }

        // Wanderer timer
        private void timer3_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                createLife();
                createAss();
            }
        }

        // Hit detection timer (doesn't seem to improve hit detection at all)
        private void Timer4_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                checkHits();
            }
        }

        // Spastic timer
        private void Timer5_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                createSpaz();
            }
        }

        // Feature: Variable Timer - Game Speeds up as score increases.
        private void Timer6_Tick(object sender, EventArgs e)
        {
            if (!gameOver && game)
            {
                checkAlien();
                alienMovement();
                singleMovement();
                spazMovement();
                moveAss();
            }
        }

        // ---------------
        //     Labels
        // ---------------

        private void Label4_Click(object sender, EventArgs e)
        {
            if (gameOver)
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Made by Yeetmeister", "Thank You for Playing C# Invaders", MessageBoxButtons.OK);
                Application.Exit();
            }
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            if (gameOver)
            {
                Application.Restart();
            }
        }
    }
}
